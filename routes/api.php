<?php
use App\Http\Controllers\AdminControllers\AdminController;
use App\Http\Controllers\AdminControllers\AgahiController;
use App\Http\Controllers\AdminControllers\DashbordController;
use App\Http\Controllers\AdminControllers\GuildController;
use App\Http\Controllers\AdminControllers\PackageController;
use App\Http\Controllers\AdminControllers\ProductController;
use App\Http\Controllers\AdminControllers\SettingController;
use App\Http\Controllers\AdminControllers\UserActionsController;
use App\Http\Controllers\AdminControllers\UserController;
use App\Http\Controllers\UserControllers\ColleageController;
use App\Http\Controllers\UserControllers\NeedController;
use App\Http\Controllers\UserControllers\SpecialController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>"admin"], function() {
    Route::post('login', [AdminController::class,'login']);
    Route::post('register', [AdminController::class,'register']);

    //routes needing authentication
    Route::group(['middleware'=>'adminAuth'],function() {
        //admin routest
        Route::get('getAdmins', [AdminController::class,'getAdmins']);
        Route::post('sendNotif', [UserController::class,'sendNotif']);

        //privilages
        Route::get('adminPrivilages/{admin_id}', [AdminController::class,'adminPrivilages']);
        Route::post('privilage', [AdminController::class,'addPrivilage']);
        Route::delete('privilage/{admin_id}/{privilage_id}', [AdminController::class,'deletePrivilage']);

        //guilds rountes
        Route::get('getGuilds', [GuildController::class,'getGuilds']);
        Route::get('getGuildsall', [GuildController::class,'getGuildsall']);

        Route::post('makeGuild', [GuildController::class,'makeGuild']);
        Route::put('activateGuild/{guild}', [GuildController::class,'activateGuild']);
        Route::put('updateGuild/{guild}', [GuildController::class,'updateGuild']);
        //dashborrd
        Route::get('dashbord/{guild_id}', [DashbordController::class,'getDashbord']);
        Route::get('agahiChart/{guild_id}', [DashbordController::class,'getAgahiChart']);


        //users
        Route::get('pendingUsers', [UserActionsController::class,'getPendingUsers']);
        Route::get('pendingDocuments', [UserActionsController::class,'getDoucmentPendingUsers']);
        Route::put('setUserState', [UserActionsController::class,'setUserstate']);

        Route::get('getUser/{user_id}', [UserController::class,'getSingleUser']);
        Route::put('updateUser/{user_id}', [UserController::class,'updateUser']);

        //products
        Route::get('getProducts/{guild_id}', [ProductController::class,'getProducts']);
        Route::post('makeProduct', [ProductController::class,'makeProduct']);
        Route::post('updateProduct/{product}', [ProductController::class,'updateProduct']);
        Route::delete('deleteProduct/{product}', [ProductController::class,'delteProduct']);

        //help
        Route::get('getHelps', [AdminController::class,'getHelps']);
        Route::post('makeHelp', [AdminController::class,'makeHelp']);
        Route::delete('deleteHelp/{help}', [AdminController::class,'deleteHelp']);
        Route::post('updateHelp/{help}', [AdminController::class,'updateHelp']);


        //setting
        Route::get('setting', [SettingController::class,'getSttting']);
        Route::post('updateSetting', [SettingController::class,'updateSetting']);
        Route::post('updateShowFull', [SettingController::class,'updateFullVersion']);

        //user_managment
        Route::get('getUsers', [UserController::class,'getUsers']);
        Route::put('activateUser', [UserController::class,'activateUser']);

        //agahi
        Route::get('getAds', [AgahiController::class,'getAllAgahies']);
        Route::get('getUnActives', [AgahiController::class,'getUnActiveAgahies']);

        Route::delete('deleteAd/{agahi_id}', [AgahiController::class,'deleteAd']);
        Route::get('getAd/{agahi_id}', [AgahiController::class,'getAd']);
        Route::put('updateAd/{agahi_id}', [AgahiController::class,'updateAd']);
        Route::put('activateAd/{agahi_id}', [AgahiController::class,'activateAd']);

        //comments
        Route::get('comments', [DashbordController::class,'getComments']);

        //comments
        Route::post('logout', [AdminController::class,'logOut']);
        Route::post('updatePassword', [AdminController::class,'updatePassword']);

        //opening hourse
        Route::get('openingHoures', [SettingController::class,'getOpeningHoures']);
        Route::put('updateOpening/{opening}', [SettingController::class,'updateOpening']);


        //packages
        Route::post('makePackage', [PackageController::class,'makePackage']);
        Route::post('updatePackage/{package}', [PackageController::class,'updatePackage']);
        Route::get('getPackages', [PackageController::class,'getPackages']);

        Route::get('watingNotfs', [PackageController::class,'getWatingNotifs']);
        Route::get('watingBanners', [PackageController::class,'getWatingBanners']);

        Route::post('notifRequestState', [PackageController::class,'notifState']);
        Route::post('bannerState', [PackageController::class,'bannerState']);

        Route::get('notifAccess', [DashbordController::class,'notifAccess']);

        Route::get('getOrders', [PackageController::class,'getOrders']);

        //discounts
        Route::post('makeDiscount', [PackageController::class,'makeDiscount']);
        Route::get('getDiscounts', [PackageController::class,'getDiscounts']);
        Route::post('updateDiscount/{discount}', [PackageController::class,'updateDiscount']);
    });

});

Route::post('register', [\App\Http\Controllers\UserControllers\UserController::class, 'register']);
Route::post('verify', [\App\Http\Controllers\UserControllers\UserController::class, 'verifyMobile']);
Route::post('login', [\App\Http\Controllers\UserControllers\UserController::class, 'login']);
Route::post('reqAgain', [\App\Http\Controllers\UserControllers\UserController::class, 'requestAgain']);
Route::get('setting', [NeedController::class, 'getSettings']);

Route::post('forgetreq', [\App\Http\Controllers\UserControllers\UserController::class, 'forgetRequest']);
Route::post('resetPassword', [\App\Http\Controllers\UserControllers\UserController::class, 'resetPassWord']);
Route::post('forgetreqAgain', [\App\Http\Controllers\UserControllers\UserController::class, 'requestAgainForget']);

Route::get('splash', [NeedController::class, 'splashNeeds']);

Route::group(['middleware'=>'userAuth'],function() {

    Route::get('profile', [\App\Http\Controllers\UserControllers\UserController::class, 'getProfile']);
    Route::post('updateProfile', [\App\Http\Controllers\UserControllers\UserController::class, 'updateProfile']);
    Route::post('updateFcmToken', [\App\Http\Controllers\UserControllers\UserController::class, 'updateFcmToken']);

    Route::get('remaining', [NeedController::class, 'getRemaningTime']);

    Route::post('uploadDocument', [\App\Http\Controllers\UserControllers\UserController::class, 'uploadDocument']);
    //agahi routes
    Route::post('uploadImage', [\App\Http\Controllers\UserControllers\AgahiController::class, 'uploadImage']);
    Route::post('makeAgahi', [\App\Http\Controllers\UserControllers\AgahiController::class, 'makeAgahi']);
    Route::delete('deletead/{agahi}', [\App\Http\Controllers\UserControllers\AgahiController::class, 'deleteAgahi']);
    Route::get('getmyAgahies', [\App\Http\Controllers\UserControllers\AgahiController::class, 'getMyAgahies']);
    Route::post('refad/{agahi}', [\App\Http\Controllers\UserControllers\AgahiController::class, 'refreshAgahi']);



    //tel routes
    Route::post('telRequst', [\App\Http\Controllers\UserControllers\UserController::class, 'telVerifyRequest']);
    Route::post('telSubmit', [\App\Http\Controllers\UserControllers\UserController::class, 'telVerifySubmit']);
    Route::post('telAgain', [\App\Http\Controllers\UserControllers\UserController::class, 'telRequestAgain']);

    //
    Route::post('report', [\App\Http\Controllers\UserControllers\AgahiController::class, 'reportAgahi']);

    //college routes
    Route::get('getColleages', [ColleageController::class, 'getColleages']);

    //need routes
    Route::get('getProducts', [NeedController::class, 'getProducts']);
    Route::post('comment', [\App\Http\Controllers\UserControllers\UserController::class, 'comment']);

    //notifs
    Route::get('getNotifs', [\App\Http\Controllers\UserControllers\UserController::class, 'getNotifs']);
    Route::get('notifUnread', [\App\Http\Controllers\UserControllers\UserController::class, 'getUnreadNotifsCount']);

    //log out
    Route::get('logout', [\App\Http\Controllers\UserControllers\UserController::class, 'logout']);

    Route::get('getDollar', [NeedController::class, 'getDollar']);

    Route::get('getPackages', [NeedController::class, 'getPackages']);
    Route::post('buyPackage', [SpecialController::class, 'buyPackage']);

    Route::post('reserveSpecial', [SpecialController::class, 'reserveSpecial']);
    Route::get('getSpecials', [SpecialController::class, 'getSpecials']);

    Route::post('reserveFori', [SpecialController::class, 'reserveFori']);
    Route::get('getFories', [SpecialController::class, 'getFories']);


    Route::post('reserveBanner', [SpecialController::class, 'reserveBanner']);
    Route::get('getBanners', [SpecialController::class, 'getBanners']);

    Route::post('reserveNotif', [SpecialController::class, 'reserveNotif']);

    //discount
    Route::post('checkDiscount', [SpecialController::class, 'checkDiscount']);




    Route::group(['middleware'=>'agahi'],function() {
        Route::get('getallAgahies', [\App\Http\Controllers\UserControllers\AgahiController::class, 'getAllAgahies']);
        Route::get('getSingleAgahi/{agahi}', [\App\Http\Controllers\UserControllers\AgahiController::class, 'getSingleAgahi']);
        Route::get('v2/getallAgahies', [\App\Http\Controllers\UserControllers\AgahiController::class, 'getAllAgahiesV2']);


    });
});

//open apis for user
Route::get('getGuilds', [\App\Http\Controllers\UserControllers\GuildController::class, 'getGuilds']);
Route::get('registerNeeds', [\App\Http\Controllers\UserControllers\UserController::class, 'getRegisterNeed']);
Route::get('updateDollar', [NeedController::class, 'updateDollar']);
Route::get('zarintest', [NeedController::class, 'zarinTest']);
Route::get('packageVerify', [SpecialController::class, 'packageVerify']);


