<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guild extends Model {


    protected $table = 'guild';
    protected $fillable=['name','active'];
    protected $primaryKey='guild_id';

    public $timestampse = false;


}
