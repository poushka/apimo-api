<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agahi_image extends Model {

    protected $table = 'agahi_image';

    protected $fillable=['name','agahi_id','user_id'];

    public $timestamps = false;

    protected $primaryKey = 'ai_id';





}
