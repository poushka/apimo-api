<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agahi extends Model {

    use SoftDeletes;
    protected $table = 'agahi';
    protected $fillable=['user_id','province_id','title','product_id','def','guild_id','type','description','pic','view','active','is_extra'];


    protected $primaryKey='agahi_id';


    public function images()
    {
        return $this->hasMany(Agahi_image::class,'agahi_id');
    }


}
