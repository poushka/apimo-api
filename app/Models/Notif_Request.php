<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Notif_Request extends Model {
    protected $table = 'notif_request';
    protected $fillable=['agahi_id','title','body','state','user_id'];
    protected $primaryKey='notif_request_id';
}
