<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;


    protected $table='users';

    protected $primaryKey ="user_id";






    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mobile',
        'name',
        'family',
        'is_employment',
        'e_name',
        'e_family',
        'e_mobile',
        'password',
        'province_id',
        'city_id',
        'address',
        'guild_id',
        'store_name',
        'state',
        'confirmed',
        'document',
        'tel_1',
        'tel_2',
        'profile',
        'mobile_visible',
        'active',
        'fcm_token',
        'notif_read'
    ];

    protected $hidden = [
        'password','fcm_token'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
