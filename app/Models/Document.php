<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model {

    protected $table = 'document';
    protected $fillable=['user_id','pic'];
    protected $primaryKey='document_id';
    public $timestamps=false;

}
