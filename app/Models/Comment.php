<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table="comment";
    public $timestamps=false;
    protected $primaryKey = "comment_id";
    protected $fillable=['rate','comment','user_id'];

}
