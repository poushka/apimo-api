<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model {
    protected $table = 'user_package';
    protected $fillable=['user_id','package_id', 'special','banner','ad','notif','fori', 'price'];
    protected $primaryKey='user_package_id';

}
