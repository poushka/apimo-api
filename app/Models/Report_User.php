<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report_User extends Model {
    public $timestamps=false;
    protected $table = 'report_user';
    protected $fillable=['report_id','agahi_id','user_id'];
    protected $primaryKey='ru_id';
}
