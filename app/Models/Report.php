<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model {
    protected $table = 'report';
    protected $fillable=['title'];
    protected $primaryKey='report_id';
}
