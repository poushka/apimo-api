<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Badge extends Model
{
    protected $table="badge";
    public $timestamps=false;
    protected $primaryKey = "badge_id";
    protected $fillable=['comment'];

}
