<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dollar extends Model
{
    //
    protected $table="dollar";
    protected $primaryKey = "dollar_id";
    protected $fillable=['price','timestamp','date'];

}
