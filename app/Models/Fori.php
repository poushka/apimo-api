<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Fori extends Model {
    protected $table = 'fori';
    protected $fillable=['agahi_id','user_id','start','end','active','guild_id'];
    protected $primaryKey='fori_id';

}
