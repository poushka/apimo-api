<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $table="city";
    public $timestamps=false;
    protected $primaryKey = "city_id";


    public function province()
    {
        return $this->belongsTo(Province::class);
    }




}
