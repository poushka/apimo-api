<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Notif extends Model {
    protected $table = 'notif';
    protected $fillable=['user_id','topic','title','body'];
    protected $primaryKey='notif_id';

}
