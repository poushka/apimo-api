<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount_User extends Model
{
    //
    protected $table="discount_user";
    protected $primaryKey ="discount_user_id";
    protected $fillable=['user_id','discount_id'];

}
