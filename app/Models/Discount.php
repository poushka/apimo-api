<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    //
    protected $table="discount";
    protected $primaryKey = "discount_id";
    protected $fillable=['code','active','usable_count','discount'];

}
