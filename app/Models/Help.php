<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Help extends Model {
    protected $table = 'help';
    protected $fillable=['title','description','url','pic'];
    protected $primaryKey='help_id';
    public    $timestampse = false;
}
