<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    protected $table="setting";
    public $timestamps=false;
    protected $primaryKey = "setting_id";
    protected $fillable=['reset','ad_limit','about','rules','expire','open'];

}
