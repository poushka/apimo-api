<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Otp_Tel extends Model {

    protected $table = 'otp_tel';
    protected $fillable=['tel','otp'];
    protected $primaryKey='otp_tel_id';

}
