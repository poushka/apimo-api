<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model {
    protected $table = 'banner';
    protected $fillable=['pic','user_id','start','end','active','guild_id','agahi_id'];
    protected $primaryKey='banner_id';

}
