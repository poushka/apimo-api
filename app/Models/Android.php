<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Android extends Model {
    protected $table = 'android';
    protected $fillable=['version','min_version','des','show_full'];
    protected $primaryKey='id';
    public $timestamps = false;
}
