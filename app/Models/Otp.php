<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Otp extends Model {

    protected $table = 'otp';
    protected $fillable=['mobile','otp'];
    protected $primaryKey='otp_id';

}
