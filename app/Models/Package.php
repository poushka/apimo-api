<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Package extends Model {
    protected $table = 'package';
    protected $fillable=['title','special','banner','ad','notif','fori', 'price'];
    protected $primaryKey='package_id';

}
