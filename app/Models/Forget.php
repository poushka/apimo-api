<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Forget extends Model {

    protected $table = 'forget';
    protected $fillable=['mobile','otp'];
    protected $primaryKey='forget_id';

}
