<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Price extends Model {
    protected $table = 'price';
    protected $fillable=['special_price'];
    protected $primaryKey='price_id';
    public    $timestampse = false;
}
