<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Order extends Model {
    protected $table = 'orders';
    protected $fillable=['package_id','user_id','state','trans_id','price','code','card','ref_id','discount_id','discounted_price'];
    protected $primaryKey='order_id';

}
