<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Opening extends Model {
    protected $table = 'opening';
    protected $fillable=['day','start','end','number','description'];
    protected $primaryKey='opening_id';
    public    $timestampse = false;
}
