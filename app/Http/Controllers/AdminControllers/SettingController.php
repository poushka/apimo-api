<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Base\BaseAdmin;
use App\Admin_Privilage;
use App\Models\Android;
use App\Models\Opening;
use App\Models\Setting;
use App\Models\User;
use App\Models\User\Agahi_image;
use App\Models\User\Category;
use App\Privilage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


session_start();

class SettingController extends BaseAdmin
{

    function getSttting(){
        $android = Android::first();
        $setting = Setting::first();
        $setting["show_full"]=$android->show_full;
        return $this->successReport($setting,"ok",200);
    }

    function  updateSetting(Request  $request) {
     $rules = [
         'ad_limit'=>'required|int',
         'about'=>'string',
         'rules'=>'string',
         'expire'=>'int',
         'open'=>'int|min:0|max:1'
     ];
     $validator = Validator::make($request->all(),$rules);
     if ($validator->fails()) {
         return $this->failureResponse($validator->errors()->first(),422);
     }
     $setting = Setting::first();
     $setting->update([
         'ad_limit'=>$request->get('ad_limit'),
         'about'=>$request->get('about'),
         'rules'=>$request->get('rules'),
         'expire'=>$request->get('expire'),
         'open'=>$request->get('open')
     ]);

     if ($setting->wasChanged()) {
         return $this->successReport([],"به روز رسانی با موفقیت انجام شد",200);
     } else
     {
         return $this->failureResponse("خطا در به روز رسانی",400);
     }
 }

    function  getOpeningHoures(Request  $request) {
      $opening = Opening::get();
      return $this->successReport($opening,"ok",200);
    }

    function  updateOpening(Request  $request,Opening $opening) {
        $rules = [
            'description'=>'required|string',
            'start'=>'required|int|min:0|max:24',
            'end'=>'required|int|min:0|max:24'

        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $opening->update([
            'start'=>$request->get('start'),
            'end'=>$request->get('end'),
            'description'=>$request->get('description'),
        ]);

        if ($opening->wasChanged()) {
            return $this->successReport([],"به روز رسانی ساعت کاری با موفقیت انجام شد",200);
        } else
        {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }
    }

    function  updateFullVersion(Request  $request) {
        $rules = [
            'show_full'=>'required|int|min:0|max:1',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $android = Android::first();

        $android->update([
            "show_full"=>$request->get('show_full')
        ]);
        if ($android->wasChanged()) {
            return $this->successReport([],"به روز رسانی با موفقیت انجام شد",200);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }

    }

}
