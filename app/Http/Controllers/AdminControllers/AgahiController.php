<?php

namespace App\Http\Controllers\AdminControllers;


use App\helper\KavehNegar;
use App\Http\Controllers\Base\BaseAdmin;
use App\Admin_Privilage;
use App\Models\Agahi;
use App\Models\Setting;
use App\Models\User;
use App\Models\User\Agahi_image;
use App\Models\User\Category;
use App\Privilage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use function App\helper\convertTime;

session_start();

class AgahiController extends BaseAdmin
{

    function getAllAgahies(Request $request) {
        $agahies = Agahi::withTrashed()->with('images');

        $setting = Setting::first();
        $time_to_expire = $setting->expire *24*60*60;

        if ($request->has("guild_id")) {
            $agahies->where("agahi.guild_id",$request->get('guild_id'));
        }
        if ($request->has("user_id")) {
            $agahies->where("agahi.user_id",$request->get('user_id'));
        }

        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";
            $agahies->where(function ($q) use ($search){
                $q->where('title', 'like', $search)
                 ->orWhere('description','like',$search)
                 ->orWhere(DB::raw('CONCAT_WS(" ", u.name, u.family)'),'like',$search)
                 ->orWhere('u.store_name','like',$search)
                 ->orWhere('u.mobile','like',$search);
            });
        }
        $agahies =  $agahies->join("guild as g","agahi.guild_id","=","g.guild_id")
            ->join('users as u','agahi.user_id','=','u.user_id')
            ->join('product as p','agahi.product_id','=','p.product_id')
            ->select('agahi.*','g.name as guild_name','u.name','u.family','u.store_name','p.name as product_name')
            ->orderBy('agahi_id','desc');

        if ($request->has('today')) {
            $agahies = $agahies->where('agahi.created_at', '>', Carbon::today())->paginate(100000);
        }else if ($request->has('user_id') || $request->has('search')) {
            $agahies = $agahies->paginate(100000);
        }else {
            $agahies = $agahies->paginate(20);
        }

        foreach ($agahies as $agahi) {
            $agahi["shamsi"]=convertTime($agahi["created_at"]);
            $created = $agahi->created_at->timestamp;
            $current = now()->timestamp;
            $diff = $current-$created;
            if ($diff>$time_to_expire) {
                $agahi["expired"]=true;
            }else {
                $agahi["expired"]=false;
            }

            }
        return $this->successReport($agahies,"ok",200);
    }

    function getUnActiveAgahies(Request $request) {
        $agahies = Agahi::where('agahi.active',0)->with('images');
        $setting = Setting::first();
        $time_to_expire = $setting->expire *24*60*60;
        if ($request->has("guild_id")) {
            $agahies->where("agahi.guild_id",$request->get('guild_id'));
        }
        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";
            $agahies->where(function ($q) use ($search){
                $q->where('title', 'like', $search)
                    ->orWhere('description','like',$search)
                    ->orWhere(DB::raw('CONCAT_WS(" ", u.name, u.family)'),'like',$search)
                    ->orWhere('u.store_name','like',$search);
            });
        }

        $agahies =  $agahies->join("guild as g","agahi.guild_id","=","g.guild_id")
            ->join('users as u','agahi.user_id','=','u.user_id')
            ->join('product as p','agahi.product_id','=','p.product_id')
            ->select('agahi.*','g.name as guild_name','u.name','u.family','u.store_name','p.name as product_name')
            ->orderBy('agahi_id','asc');


        $agahies = $agahies->get();

        foreach ($agahies as $agahi) {
            $agahi["shamsi"]=convertTime($agahi["created_at"]);
            $created = $agahi->created_at->timestamp;
            $current = now()->timestamp;
            $diff = $current-$created;
            if ($diff>$time_to_expire) {
                $agahi["expired"]=true;
            }else {
                $agahi["expired"]=false;
            }

        }
        return $this->successReport($agahies,"ok",200);
    }


    function activateUser(Request $request){
        $rules = [
            'user_id' => 'required|int',
            'active'=>'required|int|min:0|max:1'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user = User::where('user_id',$request->get('user_id'))->first();

        $user->update(['active'=>$request->get('active')]);
        if ($user->wasChanged()){
            return $this->successReport([],"عملیات با موفقیت انجام شد",200);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }
    }



    function deleteAd(Request $request,$agahi_id){
        $reaason = $request->get('reason');
        $agahi = Agahi::withTrashed()->where('agahi_id',$agahi_id)->first();
        $user = User::where('user_id',$agahi->user_id)->first();
        $agahi->update(["active"=>-1]);
        if ($agahi->wasChanged()) {
            KavehNegar::setndDeleteMessage($user->mobile, preg_replace('/\s+/', '-', $user->name . " ".$user->family), $reaason, 'delete');
            return $this->successReport([],"حذف آگهی انجام شد",200);
        }else {
            return $this->failureResponse("خطا در رد کردن آگهی",400);
        }

    }

    function getAd(Request $request,$agahi_id){
        $agahi = Agahi::withTrashed()->where("agahi_id",$agahi_id)->with('images')
            ->join("users as u","agahi.user_id",'=',"u.user_id")
            ->join("product as p","agahi.product_id","=","p.product_id")
            ->join("guild as g","agahi.guild_id","=","g.guild_id")
            ->select("agahi.*","u.name","u.family","u.mobile","u.store_name","u.tel_1","u.tel_2","u.mobile_visible","p.name as product_name","g.name as guild_name")
            ->first();
        return $this->successReport($agahi,"ok",200);



    }

    function updateAd(Request $request,$agahi_id){
        $rules = [
            'type' => 'required|int|min:1|max:2',
            'title'=>'required|string',
            'description'=>'required|string'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $agahi = Agahi::withTrashed()->where('agahi_id',$agahi_id)->first();
        $agahi->update(['type'=>$request->get('type'),
            'title'=>$request->get('title'),
            'description'=>$request->get('description')
            ]);
        return $this->successReport([],"به روز رسانی انجام شد",200);

    }

    function activateAd(Request $request,$agahi_id){
        $agahi = Agahi::withTrashed()->where('agahi_id',$agahi_id)->first();
        $agahi->update(['active'=>1]);
        return $this->successReport([],"به روز رسانی انجام شد",200);
    }




}
