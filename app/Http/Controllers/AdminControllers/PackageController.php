<?php

namespace App\Http\Controllers\AdminControllers;
use App\helper\FcmHelper;
use App\Http\Controllers\Base\BaseAdmin;
use App\Admin_Privilage;
use App\Models\Agahi;
use App\Models\Banner;
use App\Models\Discount;
use App\Models\Notif;
use App\Models\Notif_Request;
use App\Models\Order;
use App\Models\Package;
use App\Models\User;
use App\Models\User\Agahi_image;
use App\Models\User\Category;
use App\Models\UserPackage;
use App\Privilage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use function App\helper\convertTime;

session_start();

class PackageController extends BaseAdmin
{

    function makePackage(Request $request){
        $rules = [
            'title' => 'required|string',
            'special'=>'required|int',
            'banner'=>'required|int',
            'ad'=>'required|int',
            'notif'=>'required|int',
            'price'=>'required|int',
            'fori'=>'required|int'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

       $result =   Package::create([
            'title'=>$request->get('title'),
            'special'=>$request->get('special'),
            'banner'=>$request->get('banner'),
            'ad'=>$request->get('ad'),
            'notif'=>$request->get('notif'),
            'price'=>$request->get('price'),
            'fori'=>$request->get('fori')
        ]);

          if ($result){
              return  $this->successReport([],"پکیج جدید با موفقیت اضافه شد",200);
          }else {
              return  $this->successReport([],"خطا در ثبت پیکج",200);
          }


    }

    function updatePackage(Request $request,Package $package){

        $rules = [
            'title' => 'required|string',
            'special'=>'required|int',
            'banner'=>'required|int',
            'ad'=>'required|int',
            'notif'=>'required|int',
            'fori'=>'required|int',
            'price'=>'required|int',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $package->update([
            'title'=>$request->get('title'),
            'special'=>$request->get('special'),
            'banner'=>$request->get('banner'),
            'ad'=>$request->get('ad'),
            'notif'=>$request->get('notif'),
            'fori'=>$request->get('fori'),
            'price'=>$request->get('price'),
        ]);

        if ($package->wasChanged()){
            return  $this->successReport([],"به روز رسانی با موفقیت انجام شد ",200);
        }else {
            return  $this->successReport([],"خطا در ثبت به روز رسانی",200);
        }


    }


    function getPackages(Request $request){
        $packages = Package::get();
        return $this->successReport($packages,"ok",200);

    }
    function getWatingNotifs() {
        $notis = Notif_Request::where('notif_request.state',0)
            ->join('agahi as a','notif_request.agahi_id','=',"a.agahi_id")
            ->join('users as u','a.user_id','=','u.user_id')
            ->join('guild as g','a.guild_id','=','g.guild_id')
            ->select('notif_request.*', 'a.title as agahi_title','a.pic as agahi_pic','a.agahi_id','a.def' ,  'u.profile as user_profile','u.user_id','u.name','u.family','g.name as guild_name')
            ->get();
        return $this->successReport($notis,"ok",200);
    }


    function getWatingBanners() {
        $banners = Banner::where('banner.active',0)
            ->join('agahi as a','banner.agahi_id','=',"a.agahi_id")
            ->join('users as u','a.user_id','=','u.user_id')
            ->join('guild as g','a.guild_id','=','g.guild_id')
            ->select('banner.*','a.title','a.pic as agahi_pic','a.agahi_id','a.def'  ,'u.profile as user_profile','u.user_id','u.name','u.family','g.name as guild_name')
            ->get();
        return $this->successReport($banners,"ok",200);
    }
    function notifState(Request $request) {
        $rules = [
            'state' => 'required|int',
            'notif_request_id' => 'required|int',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $notif_request = Notif_Request::where("notif_request_id",$request->get("notif_request_id"))->first();
        if ($notif_request->state!=0) {
            return $this->failureResponse("وضعیت این نوتیفیکشن قبلا مشخص شده است",400);
        }


        $user = User::where('user_id', $notif_request->user_id)->first();


        $agahi = Agahi::where('agahi_id',$notif_request->agahi_id)->first();

        if ($request->get('state')==1) {
            $notif_request->update([
                'state'=>$request->get('state')
            ]);
            $body = "کاربر گرامی ، درخواست نوتیفیکیشن آگهی شما با عنوان ".$agahi->title . " به دلیل " . $request->get('reason') . " مورد تایید قرار نگرفت";
            $notif=Notif::create([
                "user_id"=>$notif_request->user_id,
                "topic"=> null,
                "title"=>"رد شدن درخواست نوتیفیکیشن",
                "body"=>$body
            ]);
            if ($user->fcm_token!=null) {
                FcmHelper::push($user->fcm_token,0,"رد شدن نوتیفیکیشن",$body);
            }

            return $this->successReport([],"نوتیفیکیشن مورد نظر با موفقیت رد گردید",200);

        }else {
            $notif_request->update([
                'state'=>$request->get('state')
            ]);
            FcmHelper::pushAgahi($user->fcm_token,0,$notif_request->title,$notif_request->body,$notif_request->agahi_id);

            $body = "کاربر گرامی ، درخواست نوتیفیکیشن آگهی شما با عنوان آگهی ".$agahi->title  . " با موفقیت ارسال گردید";

            $notif=Notif::create([
                "user_id"=>$notif_request->user_id,
                "topic"=> null,
                "title"=>"تایید شدن نوتیفیکیشن",
                "body"=>$body
            ]);

//            if ($user->fcm_token!=null) {
//                FcmHelper::push($user->fcm_token,0,"تایید شدن نوتیفیکیشن",$body);
//            }


            $user_package = UserPackage::where([["user_id",$notif_request->user_id],["notif",">",0]])->first();
            $user_package->update(["notif"=>$user_package->notif-1]);

            return $this->successReport([],"نوتیفیکیشن مورد نظر با موفقیت ارسال گردید",200);

        }
    }


    function bannerState(Request $request) {
        $rules = [
            'state' => 'required|int',
            'banner_id' => 'required|int',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $banner_requst = Banner::where("banner_id",$request->get("banner_id"))->first();
        if ($banner_requst->active!=0) {
            return $this->failureResponse("وضعیت این بنر قبلا مشخص شده است",400);
        }
        $user = User::where('user_id', $banner_requst->user_id)->first();
        $agahi = Agahi::where('agahi_id',$banner_requst->agahi_id)->first();

        if ($request->get('state')==1) {
            $banner_requst->update([
                'active'=>$request->get('state')
            ]);
            $body = "کاربر گرامی ، بنر آگهی شما با عنوان ".$agahi->title . " به دلیل " . $request->get('reason') . " مورد تایید قرار نگرفت";
            $notif=Notif::create([
                "user_id"=>$banner_requst->user_id,
                "topic"=> null,
                "title"=>"رد شدن بنر",
                "body"=>$body
            ]);
            if ($user->fcm_token!=null) {
                FcmHelper::push($user->fcm_token,0,"رد شدن بنر",$body);
            }
            return $this->successReport([],"بنر مورد نظر با موفقیت رد گردید",200);

        }else {
            $banner_requst->update([
                'active'=>$request->get('state'),
                'end'=>Carbon::now()->addHours(24)
            ]);
            $body = "کاربر گرامی ، بنر آگهی شما با عنوان ".$agahi->title  . " مورد تایید قرار گرفت";
            $notif=Notif::create([
                "user_id"=>$banner_requst->user_id,
                "topic"=> null,
                "title"=>"تایید بنر",
                "body"=>$body
            ]);

            $user_package = UserPackage::where([["user_id",$banner_requst->user_id],["banner",">",0]])->first();
            $user_package->update(["banner"=>$user_package->banner-1]);

            return $this->successReport([],"بنر مورد نظر با موفقیت تایید گردید",200);

        }
    }
    function getOrders(Request $request) {
        $orders = Order::join('users as u','orders.user_id','=','u.user_id')
        ->join("guild as g","u.guild_id","=","g.guild_id")
        ->join('package as p',"orders.package_id","=","p.package_id");



        if ($request->has("guild_id")) {
            $orders->where("u.guild_id",$request->get('guild_id'));
        }

        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";
            $orders->where(function ($q) use ($search){

                $q->where(DB::raw('CONCAT_WS(" ", u.name, family)'), 'like', $search)
                    ->orWhere('store_name','like',$search)
                    ->orWhere('mobile','like',$search)
                    ->orWhere('p.title','like',$search);
            });
        }

        $orders =   $orders->select('orders.*', 'p.*', 'u.profile', 'u.name','u.family','u.mobile' , 'g.name as guild_name')
            ->orderBy('orders.order_id','desc');

        if ($request->has('today')) {
            $orders = $orders->where('orders.created_at', '>', Carbon::today())->paginate(100000);
        }else {
            $orders = $orders->paginate(10);
        }

        foreach ($orders as $order) {
            $order["shamsi"]=convertTime($order["created_at"]);
        }




        return $this->successReport($orders,"ok",200);
    }



    function makeDiscount(Request $request) {
        $rules = [
            'code' => 'required|string',
            'usable_count' => 'required|int',
            'discount'=>'required|int'

        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $discount = Discount::where("code",$request->get('code'))->first();
        if ($discount) {
            return  $this->failureResponse("این کد تخفیف قبلا ثبت شده است",200);
        }

       $result =  Discount::create([
           "code"=>$request->get('code'),
            "usable_count"=>$request->get('usable_count'),
            "discount"=>$request->get('discount')
        ]);


       if ($result) {
           return $this->successReport($result,"کد تخفیف جدید با موفقیت اضافه شد",200);
       }else {
           return $this->failureResponse("خطا در ساخت کد تخفیف",200);
       }




    }
    function getDiscounts(){
        $discount = Discount::get();
        return $this->successReport($discount,"ok",200);
    }
    function updateDiscount(Request $request,Discount $discount){
        $rules = [
            'state' => 'required|int',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $discount->update([
           'active'=>$request->get('state')
        ]);

        if ($discount->wasChanged()) {
            return $this->successReport([],"به روز رسانی با موفقیت انجام شد",200);
        }else {
            return  $this->failureResponse("خطا در به روز رسانی",401);
        }

    }

}
