<?php

namespace App\Http\Controllers\AdminControllers;


use App\Http\Controllers\Base\BaseAdmin;
use App\Models\Admin;

use App\Models\Admin_Privilage;
use App\Models\Help;
use App\Models\Privilage;
use App\Models\Product;
use App\Models\User\Agahi_image;
use App\Models\User\Category;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Facades\JWTAuth;
use function App\helper\convertTime;

session_start();

class AdminController extends BaseAdmin
{

    function register(Request $request) {
        $rules = [
            'username' => 'required|string|max:255|unique:admin',
            'phone' => 'required',
            'password'=> 'required|min:6',
            'name'=>'required',
            'family'=>'required',
        ];
        $validator_message=['username.unique'=>"این نام کاربری قبلا ثبت شده است",
            'password.min'=>'رمز عبور باید حداقل 6 کاراکتر باشد'
        ];
        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

            $admin=  Admin::create([
                'username' => $request->get('username'),
                'phone' => $request->get('phone'),
                'password' => bcrypt($request->get('password')),
                'name'=>$request->get('name'),
                'family'=>$request->get('family'),
            ]);
            $token = JWTAuth::fromUser($admin);
            $admin["token"]=$token;
            return $this->successReport($admin,"ثبت نام با موفیت انجام گردید",201);
    }


    function login(Request $request) {
        $rules = [
            'username' => 'required',
            'password'=> 'required'
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $credentials = $request->only("username", "password");
        if (!$token = JWTAuth::attempt($credentials)) {
          return $this->failureResponse("رمز عبور یا نام کاربری صحیح نمیباشد",401);
        }else{
            $currentUser = Auth::user();
            $_SESSION["ad_login"] = true;
            $_SESSION["ad_token"] = $token;
            $data = [
                'access_token' => $token,
                'token_type' => 'bearer',
                 'admin'=>$currentUser
            ];

            return $this->successReport($data,"ورود با موفقیت انجام شد",200);
        }
    }



    function getAdmins() {
        $admins = Admin::get();

        foreach ($admins as $admin) {
            $admin["shamsi"]=convertTime($admin["created_at"]);
        }
        return $this->successReport($admins,"",200);
    }



    function adminPrivilages($id) {

        $privilages = Privilage::all();
        $admins = Admin_Privilage::where('admin_id',$id)->get();
        $admin  = Admin::where('id',$id)->first();
        $response = array();
        $response['name']=$admin['name'] . " " . $admin["family"];
        $response['privs']=array();
        foreach ($privilages as $privilage) {
              $privilage['has_priv']=false;
              foreach ($admins as $admin) {
                  if ($privilage->id==$admin->privilage_id) {
                      $privilage["has_priv"]=true;
                      break;
                  }
              }
        }
        $response['privs']=$privilages;

      return $this->successReport($response,"دریافت موفقیت آمیز",200);
    }
    function addPrivilage(Request $request) {

        $rules = [
            'admin_id' => 'required',
            'privilage_id' => 'required',

        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $priv = new Admin_Privilage();
        $priv->admin_id = $request->admin_id;
        $priv->privilage_id=$request->privilage_id;
        try {
             $priv->save();
            return $this->successReport($priv,"دسترسی با موفقیت ایجاد شد",201);

        }catch(\Exception $e) {
      return   $this->failureResponse("خطا در ایجاد درسترسی",400);
        }
    }

    function deletePrivilage(Request $request,$admin_id,$privilage_id) {

         $priv = Admin_Privilage::where([['admin_id',$admin_id],['privilage_id',$privilage_id]])->delete();
         if ($priv>0) {
             return response()->json("",204);
         }else {
             return   $this->failureResponse("خطا در حذف درسترسی",400);
         }

    }


    function getHelps(Request $request) {
      $helps = Help::get();
      return $this->successReport($helps,"ok",200);
    }

    function makeHelp(Request $request) {
        $rules = [
            'video'=> 'mimes:mpeg,ogg,mp4,webm,mov,avi,wmv|max:100040|required',
            'title' => 'required|string|max:255',
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',

        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $file = $request->video;
        $filename = time().$s. '.'.$file->extension();
        $path = public_path().'/videos/help/';

        try {
            $file->move($path, $filename);
            $pic = $request->pic;
            $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $imageName = time().$s. '.'.$pic->extension();
            $directory_pic = "images/help";
            try {
                $pic->move(public_path($directory_pic), $imageName);
            }catch (\Exception $e) {
                return  $this->failureResponse("خطا در بارگذاری تصویر",400);
            }



        }catch (\Exception $e) {
            return $this->failureResponse("خطا در بارگذاری تصویر",400);
        }
        $help = Help::create([
            "title"=>$request->get("title"),
            "pic"=>$imageName,
            "url"=>$filename
        ]);
        return $this->successReport($help,"راهنمای جدید با موفقیت ذخیره شد",201);
    }


    function updateHelp(Request $request,Help  $help) {
        $rules = [
            'title' => 'required|string|max:255',
            'video'=> 'mimes:mpeg,ogg,mp4,webm,mov,avi,wmv|max:100040'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        if ($request->exists('video')) {
            $video = $request->video;
            $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $filename = time().$s. '.'.$video->extension();
            $directory_video = "videos/help/";
            try {
                $result= $video->move(public_path($directory_video), $filename);
                $pic = $request->pic;
                $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                $imageName = time().$s. '.'.$pic->extension();
                $directory_pic = "images/help";
                try {
                    $pic->move(public_path($directory_pic), $imageName);
                }catch (\Exception $e) {
                    return  $this->failureResponse("خطا در بارگذاری تصویر",400);
                }



            }catch (\Exception $e) {
                return $this->failureResponse("خطا در بارگذاری تصویر",400);
            }
        }
        $help->update([
            "title"=>$request->get("title"),
            "url"=>$request->exists("video")? $filename : $help->url,
            "pic"=>$request->exists("video")? $imageName : $help->pic,

        ]);

        if ($help->wasChanged()) {
            return response()->json([], 204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی", 400);
        }
    }

    function deleteHelp(Request $request,Help  $help) {
        $help->delete();
        return $this->successReport([],"راهنمای مورد نظر حذف گردید",201);
    }


    function updatePassword(Request $request) {
        $rules = [
            'admin_id' => 'required|int',
            'password'=> 'required|string'
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $admin = Admin::where('id',$request->get('admin_id'))->first();
        $admin->update([ 'password' => bcrypt($request->get('password'))]);
        if ($admin->wasChanged()){
            return $this->successReport([],"رمز عبور با موفقیت تغییر کرد",200);
        }else {
            return $this->successReport([],"خطا در به روز رسانی",200);
        }
    }


    function logOut(Request $request) {
        unset($_SESSION["ad_login"]);
        unset($_SESSION["ad_token"]);
        $token = $request->header( 'Authorization' );
        JWTAuth::parseToken()->invalidate( $token );
         return $this->successReport([],"خروج با موفقیت انجام شد",200);

     }



    public function getAuthenticatedUser()
    {

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                //  if (! $user = auth('api')->parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }








}
