<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Base\BaseAdmin;
use App\Models\Guild;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;


class ProductController extends BaseAdmin
{


    function getProducts(Request $request,$guild_id) {
        $products = Product::where('guild_id',$guild_id);
        if ($request->has('search')){
            $search = "%{$request->get('search')}%";
            $products->where('name','like',$search);
        }
        return $this->successReport($products->paginate(15),"دریافت موفق",200);

    }

    function activateGuild(Request $request,Guild $guild)  {
        $rules = [
            'active' => 'required|int|min:0|max:1'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $guild->update([
            'active'=> $request->get("active")
        ]);
        if ($guild->wasChanged()) {
            return response()->json([], 204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی", 400);
        }
    }

    function makeProduct(Request $request) {
        $rules = [
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'name' => 'required|string|max:255',
            'guild_id'=>'required|int'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $pic = $request->pic;
        $date_folder = date("y-m-d");
        $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $imageName = time().$s. '.'.$pic->extension();
        $directory_pic = "images/product/$date_folder";
        $directory_thumb =  "images/product/thumb/$date_folder/";

        if (!File::exists(public_path().'/'.$directory_thumb)) {
            File::makeDirectory(public_path().'/'.$directory_thumb,0777,true);
        }
        $image = Image::make($pic)->resize(300, 300,function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path($directory_thumb . $imageName));

        try {
            $result= $pic->move(public_path($directory_pic), $imageName);
        }catch (\Exception $e) {
           return $this->failureResponse("خطا در بارگذاری تصویر",400);
        }
        $procut = Product::create([
            "name"=>$request->get("name"),
            "pic"=>$date_folder."/".$imageName,
            "guild_id"=>$request->get("guild_id")
        ]);
        return $this->successReport($procut,"محصول جدید با موفقیت ذخیره شد",201);
    }

    function updateProduct(Request $request,Product $product) {
        $rules = [
            'name' => 'required|string|max:255',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        if ($request->exists('pic')) {
            $pic = $request->pic;
            $date_folder = date("y-m-d");
            $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $imageName = time().$s. '.'.$pic->extension();
            $directory_pic = "images/product/$date_folder";
            $directory_thumb =  "images/product/thumb/$date_folder/";

            if (!File::exists(public_path().'/'.$directory_thumb)) {
                File::makeDirectory(public_path().'/'.$directory_thumb,0777,true);
            }
            $image = Image::make($pic)->resize(300, 300,function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path($directory_thumb . $imageName));

            try {
                $result= $pic->move(public_path($directory_pic), $imageName);
            }catch (\Exception $e) {
             return   $this->failureResponse("خطا در بارگذاری تصویر",400);
            }
        }
        $product->update([
            'name'=> $request->get("name"),
            'pic'=>$request->exists('pic')? $date_folder."/".$imageName : $product->pic
        ]);

        if ($product->wasChanged()) {
            return response()->json([], 204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی", 400);
        }
    }

    function delteProduct(Request $request,Product $product) {
        $pic = $product->pic;

        $result=  $product->delete();
        if ($result) {
            $filepath = "images/product/".$pic;
            @unlink($filepath);
            return response()->json([], 204);
        }else {
            return $this->failureResponse("خطا در به حذف", 400);
        }
    }






}
