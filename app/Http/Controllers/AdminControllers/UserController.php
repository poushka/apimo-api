<?php

namespace App\Http\Controllers\AdminControllers;


use App\helper\FcmHelper;
use App\Http\Controllers\Base\BaseAdmin;
use App\Models\Admin;
use App\Admin_Privilage;
use App\Models\Help;
use App\Models\Notif;
use App\Models\Product;
use App\Models\User;
use App\Models\User\Agahi_image;
use App\Models\User\Category;
use App\Privilage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Facades\JWTAuth;
use function App\helper\convertTime;

session_start();

class UserController extends BaseAdmin
{

    function getUsers(Request $request) {
        $users = User::where("state",2);

        if ($request->has("guild_id")) {
            $users->where("users.guild_id",$request->get('guild_id'));
        }

        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";
            $users->where(function ($q) use ($search){
                $q->where(DB::raw('CONCAT_WS(" ", users.name, family)'), 'like', $search)
                    ->orWhere('store_name','like',$search)
                    ->orWhere('mobile','like',$search);
            });
        }

        $users =   $users->join("province as p","users.province_id","=","p.province_id")
            ->join("city as c","c.city_id","=","users.city_id")
            ->join("guild as g","users.guild_id","=","g.guild_id")
            ->select('users.*','c.name as city_name','p.name as province_name','g.name as guild_name')
            ->orderBy('user_id','desc');

        if ($request->has('today')) {
            $users = $users->where('users.created_at', '>', Carbon::today())->paginate(100000);
        }else {
          $users = $users->paginate(10);
        }


        foreach ($users as $user) {
            $user["shamsi"]=convertTime($user["created_at"]);
            }
        return $this->successReport($users,"ok",200);
    }

    function getSingleUser(Request $request,$user_id) {
        $user = User::where("user_id",$user_id)
            ->join("guild as g","users.guild_id",'=',"g.guild_id")
            ->select("users.*",'g.name as guild_name')
            ->first();
        return $this->successReport($user,"ok",200);
    }

    function activateUser(Request $request){
        $rules = [
            'user_id' => 'required|int',
            'active'=>'required|int|min:0|max:1'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user = User::where('user_id',$request->get('user_id'))->first();

        $user->update(['active'=>$request->get('active')]);
        if ($user->wasChanged()){
            return $this->successReport([],"عملیات با موفقیت انجام شد",200);
        }else {
            return $this->failureResponse("خطا در به روز رسانی",400);
        }

    }



    function updateUser(Request $request,$user_id){

        $rules = [
            'active' => 'required|int|min:0|max:1',
            'name'=>'required|string',
            'family'=>'required|string',
            'store_name'=>'required|string',
            'address'=>'required|string',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user = User::where('user_id',$user_id)->first();
        $user->update([
            'active'=>$request->get('active'),
            'name'=>$request->get('name'),
            'family'=>$request->get('family'),
            'store_name'=>$request->get('store_name'),
            'address'=>$request->get('address'),
            'tel_1'=>$request->get('tel_1'),
            'tel_2'=>$request->get('tel_2')
        ]);
        return $this->successReport([],"به روز رسانی انجام شد",200);



    }


    function sendNotif(Request $request) {
        $rules = [
            'to' => 'string|required',
            "user_id"=>'int',
            'is_topic'=>'required|int|min:0|max:1',
            'title'=>'required|string',
            'body'=>'required|string',
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $to = $request->get('to');
        if ($request->get('is_topic')!=1) {
            $user = User::where('user_id',$request->get('user_id'))->first()->makeVisible(['fcm_token']);
            if($user['fcm_token']==null) {
                return $this->failureResponse("توکن کاربر ثبت نشده است",400);
            }
             $to=$user['fcm_token'];
        }

        $notif=Notif::create([
             "user_id"=>$request->get('is_topic')==1? null : $request->get('user_id'),
             "topic"=>$request->get('is_topic')==1? $to : null,
             "title"=>$request->get('title'),
             "body"=>$request->get('body')
        ]);
        if (!$notif) {
            return $this->failureResponse("خطا در ارسال ",400);
        }
        FcmHelper::push($to,$request->get('is_topic'),$request->get('title'),$request->get('body'));
        return  $this->successReport([],"ارسال اعلان انجام شد",200);
    }




}
