<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Base\BaseAdmin;
use App\Models\Guild;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class GuildController extends BaseAdmin
{


    function getGuilds(Request $request) {
        $guilds = Guild::get();
        return $this->successReport($guilds,"دریافت موفق",201);

    }

    function getGuildsall(Request $request) {
        $guilds = Guild::get();
        return $this->successReport($guilds,"دریافت موفق",201);

    }


    function activateGuild(Request $request,Guild $guild)  {
        $rules = [
            'active' => 'required|int|min:0|max:1'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $guild->update([
            'active'=> $request->get("active")
        ]);
        if ($guild->wasChanged()) {
            return response()->json([], 204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی", 400);
        }
    }


    function makeGuild(Request $request) {
        $rules = [
            'name' => 'required|string|max:255',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $guild = Guild::create([
            "name"=>$request->get("name")
        ]);
        return $this->successReport($guild,"صنف جدید با موفقیت ساخته شد",201);
    }

    function updateGuild(Request $request,Guild $guild) {
        $rules = [
            'name' => 'required|string|max:255',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $guild->update([
            'name'=> $request->get("name")
        ]);

        if ($guild->wasChanged()) {
            return response()->json([], 204);
        }else {
            return $this->failureResponse("خطا در به روز رسانی", 400);
        }
    }










}
