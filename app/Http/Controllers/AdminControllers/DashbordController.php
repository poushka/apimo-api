<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Base\BaseAdmin;
use App\Admin_Privilage;
use App\Models\Agahi;
use App\Models\Badge;
use App\Models\Banner;
use App\Models\Comment;
use App\Models\Guild;
use App\Models\Notif_Request;
use App\Models\User;
use App\Models\User\Agahi_image;
use App\Models\User\Category;
use App\Privilage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use function App\helper\convertTime;


session_start();

class DashbordController extends BaseAdmin
{

    function getDashbord(Request $request,$guild_id) {
        $rules = [
            'simple' => 'required|string',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        if ($request->get('simple')=="yes") {
            $response = array();
            $response["pending"] = User::where([['state',2],['state',0]])
                ->orWhere([['confirmed',1],['state',1]])
                ->count();
            $response["ads"]= Agahi::where('active',0)->count();
            $badge = Badge::first();
            $comments= Comment::count();
            $response["comments"]= $comments-$badge->comment;

            $response["banners"]= Banner::where('active',0)->count();
            $response["notifs"]= Notif_Request::where('state',0)->count();

            return $this->successReport($response,"ok",200);
        }else {
            $guild = Guild::where("guild_id",$guild_id)->first();
            $response = array();
            $response["guild_name"]=$guild["name"];
            $response["all_users"] = User::where([['state',2]])
                ->count();
            $response["guild_users"] = User::where([['state',2],['guild_id',$guild->guild_id]])
                ->count();
            $response["24_hours_users"] = User::where([['state',2],['created_at','>',Carbon::today()]])
                ->count();

            $response["24_guild_users"] = User::where([['state',2],['guild_id',$guild->guild_id],['created_at','>',Carbon::today()]])
                ->count();


            $response["all_ads"] = Agahi::count();
            $response["24_hours_ads"] = Agahi::where([['created_at','>',Carbon::today()]])
                ->count();



            $response["all_guild_ads"] = Agahi::where([['guild_id',$guild->guild_id]])
                ->count();

            $response["24_guild_ads"] = Agahi::where([['guild_id',$guild->guild_id],['created_at','>',Carbon::today()]])
                ->count();




            return $this->successReport($response,"ok",200);
        }



    }

    function getAgahiChart(Request $request,$guild_id){
        $datas = Agahi::selectRaw('Hour(created_at) as hour,COUNT(*) as count')
            ->where('guild_id',$guild_id)
            ->groupBy('hour')
            ->where('created_at', '>', Carbon::today())
            ->get();
        return $this->successReport($datas,"ok",200);
    }

    function getDoucmentPendingUsers() {
        $users = User::where('state',1)->get();
        return $this->successReport($users,'دریافت موفق',200);
    }

    function getComments(Request $request) {
        $comments = Comment::
        join('users as u','comment.user_id' , '=','u.user_id')->
            join('guild as g','u.guild_id','=','g.guild_id')->
            select('comment.*','u.name','u.mobile', 'u.family','u.user_id','u.profile','u.store_name' ,'g.name as guild_name')->
        orderBy('comment_id','desc');

        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";
            $comments->where(function ($q) use ($search){
                $q->Where(DB::raw('CONCAT_WS(" ", u.name, u.family)'),'like',$search)
                    ->orWhere('u.store_name','like',$search)
                    ->orWhere('u.mobile','like',$search);
            });
        }

        if ($request->has('search')) {
            $comments = $comments->paginate(100000);
        }else {
            $comments = $comments->paginate(20);
        }

        foreach ($comments as $comment) {
            $comment["shamsi"]=convertTime($comment["created_at"]);
        }
        $badge = Badge::first();
        $count = Comment::count();
        $badge->update(['comment'=>$count]);
        return $this->successReport($comments,"ok",200);
    }


    function notifAccess(Request $request) {
        return $this->successReport([],"ok",200);
    }


}
