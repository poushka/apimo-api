<?php

namespace App\Http\Controllers\AdminControllers;


use App\helper\Jdf;
use App\helper\KavehNegar;
use App\Http\Controllers\Base\BaseAdmin;
use App\Models\Admin;
use App\Admin_Privilage;
use App\Models\User;
use App\Models\User\Agahi_image;
use App\Models\User\Category;
use App\Privilage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Tymon\JWTAuth\Facades\JWTAuth;
use function App\helper\convertTime;

session_start();

class UserActionsController extends BaseAdmin
{
    function getPendingUsers() {
        $response = array();
        $response["pending"] = array();
        $response["document"]=array();

        $users_pending = User::where([['confirmed',1],['state',0]])
            ->join('province','users.province_id','=','province.province_id')->join('city','users.city_id','=','city.city_id')->join('guild','users.guild_id','=','guild.guild_id')
            ->select('users.*','province.name as province_name','city.name as city_name','guild.name as guild_name')
            ->orderBy('user_id','desc')
            ->get();
        foreach ($users_pending as $user) {
            $user["shamsi"]=convertTime($user["created_at"]);
            array_push($response["pending"],$user);
        }
        $users_pending_document = User::where([['confirmed',1],['state',1]])
            ->join('province','users.province_id','=','province.province_id')->join('city','users.city_id','=','city.city_id')->join('guild','users.guild_id','=','guild.guild_id')
            ->select('users.*','province.name as province_name','city.name as city_name','guild.name as guild_name')
            ->orderBy('user_id','desc')
            ->get();
        foreach ($users_pending_document as $user) {
            $user["shamsi"]=convertTime($user["created_at"]);
            array_push($response["document"],$user);
        }


        return $this->successReport($response,'دریافت موفق',200);
    }
    function getDoucmentPendingUsers() {
        $users = User::where('state',1)->get();
        return $this->successReport($users,'دریافت موفق',200);
    }
    function setUserstate(Request $request) {
        $rules = [
            'state' => 'required|int',
            'user_id' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $state = $request->get('state');
        $user = User::where('user_id',$request->get('user_id'))->first();
        $user->update(['state'=>$state]);

        if ($user->wasChanged()) {
            if ($state==-1) {
                KavehNegar::sendOtpMessage($user->mobile,preg_replace('/\s+/', '-', $user->name . " ".$user->family),'reject');
                $user->delete();
                return  $this->successReport([],"درخواست عضویت رد شد ",200);
            }else if ($state==1) {
                KavehNegar::sendOtpMessage($user->mobile,preg_replace('/\s+/', '-', $user->name . " ".$user->family),'document');
                return  $this->successReport([],"درخواست آپلود مدرک ارسال شد ",200);
            }else if ($state==2){
                KavehNegar::sendOtpMessage($user->mobile,preg_replace('/\s+/', '-', $user->name . " ".$user->family),'acept');
                return  $this->successReport([],"تایید کاربر با موفقیت انجام شد",200);
            }

        } else {
            return  $this->failureResponse("خطا در تغییر وضعیت . دوباره تلاش کنید",400);
        }


    }



}
