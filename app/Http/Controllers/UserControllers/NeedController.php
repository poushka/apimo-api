<?php

namespace App\Http\Controllers\UserControllers;
use App\helper\KavehNegar;
use App\Http\Controllers\Base\BaseUser;
use App\Models\Agahi;
use App\Models\Android;
use App\Models\Dollar;
use App\Models\Help;
use App\Models\Opening;
use App\Models\Package;
use App\Models\Product;
use App\Models\Setting;
use App\Models\UserPackage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function App\helper\convertTime;
use function App\helper\convertTimeJustDate;


class NeedController extends BaseUser

{
    function getProducts(Request $request)
    {
        $response = array();
        $response["products"] = array();
        global $guild_id;
        global $user_id;

        $day = Carbon::now()->dayOfWeekIso;

        $opening = Opening::where('number', $day)->first();
        if ($opening->start == 1 && $opening->end == 1) {
            return $this->failureResponse($opening->description, 403);
        } else if ($opening->start != 0 || $opening->end != 0) {
            $current_hour = Carbon::now()->hour;
            if ($current_hour < $opening->start || $current_hour >= $opening->end) {
                return $this->failureResponse($opening->description, 403);
            }
        }
        $setting = Setting::first();
        $last_agahi = Agahi::where([['user_id',$user_id],['is_extra',0]])->orderBy('created_at', 'desc')->first();
        if (!$last_agahi == null && $setting->ad_limit > 0) { //here means  that he has add list one agahi
            $created = $last_agahi->created_at->timestamp;;
            $current = now()->timestamp;
            $diff = $current - $created;
            $limit = $setting->ad_limit * 60;
            $response["diff"] = $limit - $diff;

        } else {
            $response["diff"] = 0;
        }

        $products = Product::where("guild_id", $guild_id)
            ->orderBy('name', 'asc')
            ->get();
        foreach ($products as $product) {
            array_push($response["products"], $product);
        }

        return $this->successReport($response, "ok", 200);


    }

    function splashNeeds(Request $request)
    {
        $response = array();
        $android = Android::first();
        $response["android"] = $android;
        return $this->successReport($response, "succes", 200);
    }

    function getSettings()
    {
        $help = Help::get();
        $setting = Setting::first();
        $setting["helps"] = $help;
        return $this->successReport($setting, "ok", 200);
    }

    function getRemaningTime()
    {
        global $user_id;
        $setting = Setting::first();
        $last_agahi = Agahi::where([['user_id',$user_id],['is_extra',0]])->orderBy('created_at', 'desc')->first();
        if (!$last_agahi == null && $setting->ad_limit > 0) { //here means  that he has add list one agahi
            $created = $last_agahi->created_at->timestamp;;
            $current = now()->timestamp;
            $diff = $current - $created;
            $limit = $setting->ad_limit * 60;
            return $this->successReport($limit - $diff, "ok", 200);


        } else {
            return $this->successReport(0, "ok", 200);
        }

    }

    function getDollar()
    {
        $dollar = Dollar::first();
        $date = date("Y/m/d");
        $dollar["shamsi"]=convertTimeJustDate($date);
        return $this->successReport($dollar, "ok", 200);
    }

    function updateDollar()
    {
        $data = KavehNegar::updateDollar();
        $price = $data['harat_naghdi_buy']['value'];
        $timestamp = $data['harat_naghdi_buy']['timestamp'];
        $date = $data['harat_naghdi_buy']['date'];
        $dollar = Dollar::first();
        $dollar->update(['price' => $price, 'timestamp' => $timestamp, 'date' => $date]);
        return $this->successReport([], "updated", 200);
    }

    function zarinTest()
    {
        $data = array("merchant_id" => "0bd3b1e8-fd45-42ac-8696-eb9c05567162",
            "amount" => 1000,
            "callback_url" => "http://localhost/verify.php",
            "description" => "خرید تست",
            "metadata" => ["email" => "info@email.com", "mobile" => "09121234567"],
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://api.zarinpal.com/pg/v4/payment/request.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v4');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));

        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true, JSON_PRETTY_PRINT);
        curl_close($ch);

        if ($err) {
            echo "cURL Error #:" . $err;

        } else {
            if (empty($result['errors'])) {
                if ($result['data']['code'] == 100) {
                    $path = "https://www.zarinpal.com/pg/StartPay/" . $result['data']["authority"];
                    return redirect()->to($path);
                    // header('Location: https://www.zarinpal.com/pg/StartPay/' . $result['data']["authority"]);
                }
            } else {

                echo 'Error Code: ' . $result['errors']['code'];
                echo 'message: ' . $result['errors']['message'];

            }
        }
    }

    function getPackages()
    {
        global $user_id;
        $response = array();
        $response["packages"]=array();
        $is_there_package = UserPackage::where('user_id', $user_id)->first();
        if ( !$is_there_package) {
           UserPackage::create([
               "user_id"=>$user_id,
               "package_id"=>null,
               "special"=> 0,"ad"=>0,"banner"=>0,"notif"=>0,"price"=>0
           ]);
        }

        $my_packages = UserPackage::where('user_id', $user_id)->
        select([DB::raw("SUM(banner) as banner"), DB::raw("SUM(ad) as ad"),
            DB::raw("SUM(special) as special"),DB::raw("SUM(notif) as notif"),
            DB::raw("SUM(fori) as fori")
            ])
            ->groupBy('user_id')->first();



        $packages = Package::get();

        $response["packages"]=$packages;
        $response["my_packages"]=$my_packages;


        return $this->successReport($response,"ok",200);


    }
}
