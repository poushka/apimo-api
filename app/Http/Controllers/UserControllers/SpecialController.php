<?php

namespace App\Http\Controllers\UserControllers;
use App\helper\FcmHelper;
use App\helper\KavehNegar;
use App\Http\Controllers\Base\BaseUser;
use App\Models\Agahi;
use App\Models\Android;
use App\Models\Banner;
use App\Models\Discount;
use App\Models\Discount_User;
use App\Models\Dollar;
use App\Models\Fori;
use App\Models\Help;
use App\Models\Notif_Request;
use App\Models\Opening;
use App\Models\Order;
use App\Models\Package;
use App\Models\Price;
use App\Models\Product;
use App\Models\Province;
use App\Models\Setting;
use App\Models\Special;
use App\Models\User;
use App\Models\UserPackage;
use Carbon\Carbon;
use Doctrine\Inflector\CachedWordInflector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use function App\helper\convertTime;


class SpecialController extends BaseUser

  {
    function reserveSpecial(Request $request) {
         global $user_id;
         global $guild_id;
         $rules = [
             'agahi_id' => 'required|int',
         ];
         $validator = Validator::make($request->all(),$rules);
         if ($validator->fails()) {
             return $this->failureResponse($validator->errors()->first(),422);
         }
        $user_package = UserPackage::where('user_id', $user_id)->
        select([DB::raw("SUM(banner) as banner"), DB::raw("SUM(ad) as ad"),
            DB::raw("SUM(special) as special"),DB::raw("SUM(notif) as notif")
        ])->groupBy('user_id')->first();
         if ($user_package==null) {
             return $this->failureResponse("کاربر گرامی، برای ثبت آگهی ویژه ابتدا باید پکیج تهیه نمایید",403);
         }

         if ($user_package->special < 1) {
             return $this->failureResponse("کاربر گرامی، برای ثبت آگهی ویژه ابتدا باید پکیج تهیه نمایید",403);
         }
         $agahi_id = $request->get('agahi_id');

         $is_special = Special::where([['agahi_id',$agahi_id],['end' , '>' ,Carbon::now()]])->first();
         if ($is_special) {
             return  $this->failureResponse("آگهی مورد نظر در حال حاضر ویژه میباشد",400);
         }
                     $special = Special::create([
                         'agahi_id'=>$agahi_id,
                         'user_id'=>$user_id,
                         'guild_id'=>$guild_id,
                         'active'=>1,
                         'start'=>Carbon::now(),
                         'end'=>Carbon::now()->addHours(24)
                     ]);
                     if (!$special) {
                         return $this->failureResponse("خطا در ثبت . لطفا دوباره تلاش کنید",400);
                     }
                     $user_package = UserPackage::where([["user_id",$user_id],["special",">",0]])->first();
                     $user_package->update(["special"=>$user_package->special-1]);
                     return $this->successReport($special,"آگهی مورد نظر با موفقیت در جایگاه ویژه ثبت گردید",200);

     }
    function getSpecials(Request $request) {
        global $guild_id;
        global $province_id;

        $specials = Special::where([['special.guild_id',$guild_id],['end' , '>' ,Carbon::now()],
            ['a.province_id',$province_id]
        ])
            ->join('agahi as a','special.agahi_id',"=",'a.agahi_id')
            ->join("users as u",'a.user_id','=','u.user_id')
            ->join("product as p","a.product_id","=","p.product_id")
            ->select('a.*','u.name','u.family',"u.profile", "p.name as product_name");


        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";
            $specials->where(function ($q) use ($search){
                $q->where(DB::raw('a.title'), 'like', $search)
                    ->orWhere('a.description','like',$search)
                    ->orWhere('p.name','like',$search);
            });
        };

        $specials = $specials->orderBy('special.special_id','desc')->paginate(10);
        foreach ($specials as $agahi) {
            $agahi["shamsi"]=convertTime($agahi["created_at"]);
        }


        return $this->successReport($specials->items(),"ok",200);

    }


    function reserveFori(Request $request) {
        global $user_id;
        global $guild_id;
        $rules = [
            'agahi_id' => 'required|int',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user_package = UserPackage::where('user_id', $user_id)->
        select([DB::raw("SUM(fori) as fori")

        ])->groupBy('user_id')->first();
        if ($user_package==null) {
            return $this->failureResponse("کاربر گرامی، برای ثبت آگهی فوری ابتدا باید پکیج فوری تهیه نمایید",403);
        }

        if ($user_package->fori < 1) {
            return $this->failureResponse("کاربر گرامی، برای ثبت آگهی فوری ابتدا باید پکیج فوری تهیه نمایید",403);
        }
        $agahi_id = $request->get('agahi_id');

        $is_fori = Fori::where([['agahi_id',$agahi_id],['end' , '>' ,Carbon::now()]])->first();
        if ($is_fori) {
            return  $this->failureResponse("آگهی مورد نظر در حال حاضر فوری میباشد",400);
        }
        $fori = Fori::create([
            'agahi_id'=>$agahi_id,
            'user_id'=>$user_id,
            'guild_id'=>$guild_id,
            'active'=>1,
            'start'=>Carbon::now(),
            'end'=>Carbon::now()->addHours(24)
        ]);
        if (!$fori) {
            return $this->failureResponse("خطا در ثبت . لطفا دوباره تلاش کنید",400);
        }
        $user_package = UserPackage::where([["user_id",$user_id],["fori",">",0]])->first();
        $user_package->update(["fori"=>$user_package->fori-1]);
        return $this->successReport($fori,"آگهی مورد نظر با موفقیت در جایگاه فوری ثبت گردید",200);

    }
    function getFories(Request $request) {
        global $guild_id;
        global $province_id;

        $fories = Fori::where([['fori.guild_id',$guild_id],['end' , '>' ,Carbon::now()],
            ['a.province_id',$province_id]
        ])
            ->join('agahi as a','fori.agahi_id',"=",'a.agahi_id')
            ->join("users as u",'a.user_id','=','u.user_id')
            ->join("product as p","a.product_id","=","p.product_id")
            ->select('a.*','u.name','u.family',"u.profile", "p.name as product_name")
            ->orderBy('fori_id',"desc")
            ->get();
        foreach ($fories as $agahi) {
            $agahi["shamsi"]=convertTime($agahi["created_at"]);
        }

        return $this->successReport($fories,"ok",200);

    }

    function reserveBanner(Request $request) {
        global $user_id;
        global $guild_id;
        $rules = [
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'agahi_id' => 'required|int',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user_package = UserPackage::where('user_id', $user_id)->
        select([DB::raw("SUM(banner) as banner")
        ])->groupBy('user_id')->first();
        if ($user_package==null) {
            return $this->failureResponse("کاربر گرامی، برای ثبت بنر  ابتدا باید پکیج تهیه نمایید",403);
        }
        if ($user_package->banner < 1) {
            return $this->failureResponse("کاربر گرامی، برای ثبت بنر  ابتدا باید پکیج تهیه نمایید",403);
        }


        $isWating = Banner::where([['agahi_id',$request->get('agahi_id')],[
            'active',0]])->first();
        if ($isWating) {
            return  $this->failureResponse("در حال حاضر یک بنر در انتظار تایید برای این آگهی وجود دارد",400);
        }


        $hasBanner = Banner::where([['agahi_id',$request->get('agahi_id')],['end' , '>' ,Carbon::now()],[
            'active',2]])->first();


        if ($hasBanner) {
            return  $this->failureResponse("آگهی مورد نظر در حال حاضر حاضر دارای بنر میباشد",400);
        }


        $pic = $request->pic;
        $date_folder = date("y-m-d");
        $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $imageName = time().$s. '.'.$pic->extension();
        $directory_pic = "images/banner/$date_folder";
        try {
            $result= $pic->move(public_path($directory_pic), $imageName);
        }catch (\Exception $e) {
            return $this->failureResponse("خطا در بارگذاری تصویر",400);
        }
        $banner = Banner::create([
            'pic'=> $date_folder."/".$imageName,
            'agahi_id'=>$request->get('agahi_id'),
            'user_id'=>$user_id,
            'guild_id'=>$guild_id,
            'active'=>0,
            'start'=>Carbon::now(),
            'end'=>Carbon::now()->addHours(24)
        ]);
        if (!$banner) {
            return $this->failureResponse("خطا در ثبت  . لطفا دوباره تلاش کنید",400);
        }
        return $this->successReport($banner,"بنر شما با موفقیت ثبت گردید و پس از تایید به نمایش در خواهد آمد و موجودی بنر شما کسر خواهد شد",200);

    }
    function getBanners(Request $request) {
        global $guild_id;
        global $province_id;
        $banners = Banner::where([['banner.guild_id',$guild_id],['end' , '>' ,Carbon::now()],['banner.active',2],
            ['u.province_id',$province_id]
             ])
            ->join("users as u",'banner.user_id','=','u.user_id')
            ->select('banner.*','u.name','u.family',"u.profile")
            ->orderBy('banner_id',"desc")
            ->get();
        return $this->successReport($banners,"ok",200);

    }
    function reserveNotif(Request $request) {
        global $user_id;
        $rules = [
            'agahi_id' => 'required|int',
            'title'=>'required|string',
            'body'=>'required|string',

        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user_package = UserPackage::where('user_id', $user_id)->
        select([DB::raw("SUM(notif) as notif")

        ])->groupBy('user_id')->first();
        if ($user_package==null) {
            return $this->failureResponse("کاربر گرامی، برای ارسال نوتیفیکیشن ابتدا باید پکیج نوتیفیکیشن تهیه نمایید",403);
        }

        if ($user_package->notif < 1) {
            return $this->failureResponse("کاربر گرامی، برای ارسال نوتیفیکیشن ابتدا باید پکیج نوتیفیکیشن تهیه نمایید",403);
        }
        $agahi_id = $request->get('agahi_id');
        $has_notif = Notif_Request::where([['agahi_id',$agahi_id],['state',0]])->first();
        if ($has_notif) {
            return  $this->failureResponse("در حال حاضر یک درخواست  نوتیفیکیشن در انتظار تایید برای این آگهی وجود دارد",400);
        }
        $noti_request = Notif_Request::create([
            'agahi_id'=>$agahi_id,
            'title'=>$request->get('title'),
            'body'=>$request->get('body'),
            'user_id'=>$user_id
        ]);
        if (!$noti_request) {
            return $this->failureResponse("خطا در ثبت . لطفا دوباره تلاش کنید",400);
        }
        return $this->successReport($noti_request,"درخواست ارسال نوتیفیکیشن با موفقیت ثبت و بعد از تایید تیم پشتیبانی ارسال خواهد شد و موجودی نوتیفیکیشن شما پس از تایید کسر خواهد شد",200);

    }



    function buyPackage(Request $request) {
        $request_address = "https://api.zarinpal.com/pg/v4/payment/request.json";
        $callback = $this->isLocalhost() ? "http://localhost/apimo/public/api/packageVerify" : "http://apimo.ir/api/public/api/packageVerify";
        global $user_id;
        $user = User::where('user_id',$user_id)->first();
        $discount=null;

        $rules = [
            'package_id' => 'required|int',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        //check disount code
        if ($request->exists('code')) {
            $discount = Discount::where('code',$request->get('code'))->first();
            if (!$discount) {
                return  $this->failureResponse("این کد تخفیف وجود ندارد",404);
            }
            if ($discount->active==0) {
                return  $this->failureResponse("این کد تخفیف منقضی شده است",400);
            }
            $used_count = Discount_User::where([['user_id',$user_id],['discount_id',$discount->discount_id]])->count();

            if ($used_count>=$discount->usable_count) {
                return  $this->failureResponse("سقف دفعات استفاده شما از این کد تخفیف به پایان رسیده است",400);
            }
        }


        $package_id = $request->get('package_id');
        $package = Package::where('package_id',$package_id)->first();
        $data = array("merchant_id" => "0bd3b1e8-fd45-42ac-8696-eb9c05567162",
            "amount" => $package->price,
            "callback_url" => $callback,
            "description" => "خرید " . $package->title,
            "metadata" => ["mobile"=>$user->mobile],
        );
        $jsonData = json_encode($data);
        $ch = curl_init($request_address);
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));

        $result = curl_exec($ch);
        $err = curl_error($ch);

        $result = json_decode($result, true, JSON_PRETTY_PRINT);
        curl_close($ch);
        if ($err) {
            return $this->failureResponse("cURL Error #:" . $err,400) ;
        } else {
            if (empty($result['errors'])) {
                if ($result['data']['code'] == 100) {
                    $authority = $result['data']["authority"];
                    if ($discount==null) {
                        $discounted_price=null;
                    }else {
                        $percent  = ($package->price * $discount->discount) /100;
                        $discounted_price = $package->price-$percent;
                    }

                    $order = Order::create([
                        'package_id'=>$package_id,
                        'user_id'=>$user_id,
                        'trans_id'=>$authority,
                        'price'=> $package->price,
                        'discount_id'=>$discount==null ? null : $discount->discount_id,
                        'discounted_price'=>$discounted_price
                    ]);
                    if (!$order) {
                        return $this->failureResponse("خطا در ثبت . لطفا دوباره تلاش کنید",400);
                    }
                    return $this->successReport([],$authority,200);

                }
            } else {
                return $this->failureResponse( 'Error Code: ' . $result['errors']['code'] . "\n" . 'message: ' .  $result['errors']['message'] ,400) ;
            }
        }
    }
    function packageVerify(Request $request) {
        $request_address = "https://api.zarinpal.com/pg/v4/payment/verify.json";
        if ($this->isLocalhost()) {
            $pay_result_address = "http://localhost/apimo/public/payr?data=";
        }else {
            $pay_result_address = "https://apimo.ir/api/public/payr?data=";
        }
        $authority = $request->get('Authority');
        $order = Order::where('trans_id',$authority)->first();
        if (!$order) {
            $data = $this->getPayDat("نتیجه پرداخت خرید پکیج","کد تراکنش مورد نظر یافت نشد",0,"android");
            return redirect()->to($pay_result_address.$data)->send();
        }
        $status = $request->get('Status');
        if ($status=="NOK") {
            $data = $this->getPayDat("نتیجه پرداخت خرید پکیج","خطا در پرداخت ",0,"android");
            $order->update(['state'=> 2,  "code"=>0]); // yani laghv shode
            return redirect()->to($pay_result_address.$data)->send();

        }else {
        $data = array("merchant_id" => "0bd3b1e8-fd45-42ac-8696-eb9c05567162",
             "amount" => $order->price,
             "authority"=>$authority
        );
        $jsonData = json_encode($data);
        $ch = curl_init($request_address);
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v4');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));
            $result = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
            $result = json_decode($result, true);
            if ($err) {
                $data = $this->getPayDat("نتیجه پرداخت خرید پکیج","خطا در استعلام " . "cURL Error #:" . $err ,0,"android");
                return redirect()->to($pay_result_address.$data)->send();

            } else {
                if ($result['data']['code'] == 100 || ($result['data']['code'] == 101 && $order->state !=6)) {
                   $package = Package::where('package_id',$order->package_id)->first();
                   $user_package = UserPackage::create([
                       "user_id"=>$order->user_id,
                       "package_id"=> $order->package_id,
                       'special'=>$package->special,
                       'banner'=>$package->special,
                       'ad'=>$package->special,
                       'notif'=>$package->special,
                       'fori'=>$package->fori,
                       'price'=>$order->price,
                   ]);
                     if (!$user_package) {
                         $data = $this->getPayDat("نتیجه پرداخت خرید پکیج","خطا در ثبت بسته . "  . $package->title ,1,"android");
                         $order->update(['state'=> 1,  "code"=>100]);
                         return redirect()->to($pay_result_address.$data)->send();
                     }
                    $order->update(['state'=> 6,"code"=>100,
                           'card'=>$result['data']['card_pan'],
                            'ref_id'=>$result['data']['ref_id']
                        ]);
                    if ($order->discount_id!=null) {
                        Discount_User::create([
                           'user_id'=>$order->user_id,
                           'discount_id'=> $order->discount_id
                        ]);
                    }
                    $data = $this->getPayDat("نتیجه پرداخت خرید پکیج","خرید "  . $package->title . " با موفقیت انجام شد",1,"android");
                    return redirect()->to($pay_result_address.$data)->send();

                } else if ($result['data']['code'] == 101 && $order->state ==6) {
                    $data = $this->getPayDat("نتیجه پرداخت خرید پکیج","تراکنش مورد نظر قبلا ثبت شده است",0,"android");
                    return redirect()->to($pay_result_address.$data)->send();
                }else {
                    $order->update([
                        "code"=>$result['errors']['code']
                    ]);
                    $data = $this->getPayDat("نتیجه پرداخت خرید پکیج","خطا در ثبت بسته . "  . $result['errors']['message'] ,1,"android");
                    $order->update(['state'=> 1,  "code"=>100]);
                    return redirect()->to($pay_result_address.$data)->send();
                }
            }

      }
    }


    function checkDiscount(Request $request){
        global $user_id;
        $rules = [
            'code' => 'required|string',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $discount = Discount::where('code',$request->get('code'))->first();
        if (!$discount) {
            return  $this->failureResponse("این کد تخفیف وجود ندارد",404);
        }
        if ($discount->active==0) {
            return  $this->failureResponse("این کد تخفیف منقضی شده است",400);
        }
        $used_count = Discount_User::where([['user_id',$user_id],['discount_id',$discount->discount_id]])->count();

        if ($used_count>=$discount->usable_count) {
            return  $this->failureResponse("سقف دفعات استفاده شما از این کد تخفیف به پایان رسیده است",400);
        }

        return $this->successReport($discount->discount,"قابل استفاده",200);



    }




    function isLocalhost($whitelist = ['127.0.0.1', '::1','192.168.1.9']) {
        return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
    }
    function getPayDat($title , $message , $state , $aoi) {
        $result = array("title"=>$title,"message"=>$message , "state"=>$state,"aoi"=>$aoi);
        return base64_encode(json_encode($result));
    }

   }
