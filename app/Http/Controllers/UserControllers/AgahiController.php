<?php

namespace App\Http\Controllers\UserControllers;
use App\Http\Controllers\Base\BaseUser;
use App\Models\Agahi;
use App\Models\Agahi_image;
use App\Models\Fori;
use App\Models\Opening;
use App\Models\Product;
use App\Models\Report;
use App\Models\Report_User;
use App\Models\Setting;
use App\Models\UserPackage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use function App\helper\convertTime;


class AgahiController extends BaseUser
{

    function uploadImage(Request $request) {
        global $user_id;
        $rules = [
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'identifier'=>'required|string'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $pic = $request->pic;
        $date_folder = date("y-m-d");
        $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

        $imageName = time().$s. '.'.$pic->extension();
        $directory_thumb =  "images/agahi/thumb/$date_folder/";
        $directory_pic = "images/agahi/pic/$date_folder";

        if (!File::exists(public_path().'/'.$directory_thumb)) {
            File::makeDirectory(public_path().'/'.$directory_thumb,0777,true);
        }
        $image = Image::make($pic)->resize(300, 300,function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path($directory_thumb . $imageName));

        try {
             $pic->move(public_path($directory_pic), $imageName);
        }catch (\Exception $e) {
          return  $this->failureResponse("خطا در بارگذاری تصویر",400);
        }


        $agahi_image = new Agahi_image();
        $agahi_image->name=$date_folder.'/'.$imageName;
        $agahi_image->user_id=$user_id;
        $agahi_image->save();
        return $this->successReport(['ai_id'=>$agahi_image->ai_id,'identifier'=>$request->identifier,'name'=>$date_folder.'/'.$imageName
        ],"آپلود موفقیت آمیز",200);
    }

    function makeAgahi(Request  $request) {





        global $user_id;
        global $guild_id;
        global $province_id;




        $rules = [
            'title' => 'required|string|max:255',
            'product_id'=>'required|int',
            'description'=>'required|string',
            'type'=>'int|min:1|max:2',
            'def'=>'int|min:0|max:1'
        ];
        $images = $request->get('images');   //"images":[{"ai_id":1,"name":"20-07-13/1594674813.jpeg"}]
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $day = Carbon::now()->dayOfWeekIso;
        $opening = Opening::where('number',$day)->first();
        if ($opening->start==1 && $opening->end==1) {
            return $this->failureResponse($opening->description,403);
        }else if ($opening->start!=0 || $opening->end!=0) {
            $current_hour = Carbon::now()->hour;
            if ($current_hour < $opening->start || $current_hour>=$opening->end) {
                return $this->failureResponse($opening->description,403);
            }
        }
        $setting = Setting::first();
        $last_agahi = Agahi::where([['user_id',$user_id],['is_extra',0]])->orderBy('created_at','desc')->first();
        if (!$last_agahi==null && $setting->ad_limit>0) { //here means  that he has add list one agahi
            $created = $last_agahi->created_at->timestamp;;
            $current = now()->timestamp;
            $diff = $current-$created;
            $limit = $setting->ad_limit * 60 ;
            if ($diff<$limit) {
                if ($request->exists("use_package")) {
                    $user_package = UserPackage::where('user_id', $user_id)->
                    select([DB::raw("SUM(banner) as banner"), DB::raw("SUM(ad) as ad"),
                        DB::raw("SUM(special) as special"),DB::raw("SUM(notif) as notif")
                    ])->groupBy('user_id')->first();

                    if (!$user_package) {
                        return $this->failureResponse("پکیج فعالی یافت نشد .",400);
                    }
                    if ($user_package->ad == 0 ) {
                        return $this->failureResponse("پکیج فعالی یافت نشد .",400);
                    }
                }else {
                    return $this->failureResponse("محدودیت زمانی شما برای ثبت آگهی به پایان نرسیده است",400);
                }
            }
        }


        //then we create agahi
        $agahi=  Agahi::create([
             "title"=>$request->get("title"),
             "user_id"=>$user_id,
             "province_id"=>$province_id,
             "product_id"=>$request->get("product_id"),
             "description"=>$request->get("description"),
             "type"=>$request->get("type"),
             "guild_id"=>$guild_id,
             "def"=>$request->get("def"),
             "is_extra"=>$request->exists("use_package") ? 1 : 0
        ]);
        if ($request->get('def')==0) {
            $ai_ids = array_column($images, 'ai_id');
            Agahi_image::whereIn('ai_id', $ai_ids)->update(['agahi_id'=>$agahi->agahi_id]);
            $agahi->update(["pic"=>$images[0]["name"]]);
        }else {
            $prouct = Product::where("product_id",$request->get("product_id"))->first();
            $agahi->update(["pic"=>$prouct->pic]);
        }
        if ($request->exists("use_package")) {
            $user_package = UserPackage::where([["user_id",$user_id],["ad",">",0]])->first();
            $user_package->update(["ad"=>$user_package->ad-1]);
        }
        return $this->successReport($agahi,"آگهی با موفقیت ثبت گردید",201);
    }

    function getAllAgahies(Request  $request) {
        global $guild_id;
        global $province_id;

        $setting = Setting::first();
        $agahies = Agahi::join("users as u",'agahi.user_id','=','u.user_id')
            ->join("product as p","agahi.product_id","=","p.product_id")
            ->select('agahi.*','u.name','u.family',"p.name as product_name");
      $agahies->where("agahi.guild_id",$guild_id);
      $agahies->where("agahi.active",'>=',0);
      $agahies->where('agahi.province_id',$province_id);
      $agahies->where('agahi.created_at','>=', DB::raw("DATE_SUB(NOW(), INTERVAL $setting->expire day)"));

      if ($request->has('type')) {
          $agahies->where("agahi.type",$request->get('type'));
      }
        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";
            $agahies->where(function ($q) use ($search){
                $q->where(DB::raw('agahi.title'), 'like', $search)
                    ->orWhere('agahi.description','like',$search)
                    ->orWhere('p.name','like',$search);
            });
        }


        $agahies = $agahies->orderBy('agahi_id','desc')->paginate(10);
        foreach ($agahies as $agahi) {
            $agahi["shamsi"]=convertTime($agahi["created_at"]);
        }
        return $this->successReport($agahies->items(),"موفق",200);
   }

    function getAllAgahiesV2(Request  $request) {
        global $guild_id;
        global $province_id;

        $response=array();
        $setting = Setting::first();
        $agahies = Agahi::join("users as u",'agahi.user_id','=','u.user_id')
            ->join("product as p","agahi.product_id","=","p.product_id")
            ->select('agahi.*','u.name','u.family',"p.name as product_name");
        $agahies->where("agahi.guild_id",$guild_id);
        $agahies->where("agahi.active",'>=',0);
        $agahies->where('agahi.province_id',$province_id);
        $agahies->where('agahi.created_at','>=', DB::raw("DATE_SUB(NOW(), INTERVAL $setting->expire day)"));

        if ($request->has('type')) {
            $agahies->where("agahi.type",$request->get('type'));
        }
        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";
            $agahies->where(function ($q) use ($search){
                $q->where(DB::raw('agahi.title'), 'like', $search)
                    ->orWhere('agahi.description','like',$search)
                    ->orWhere('p.name','like',$search);
            });
        }


        $agahies = $agahies->orderBy('agahi_id','desc')->paginate(10);
        foreach ($agahies as $agahi) {
            $agahi["shamsi"]=convertTime($agahi["created_at"]);
        }
        $response["agahies"]=$agahies->items();

        if ($request->get('page')==1) {
            $fories = Fori::where([['fori.guild_id',$guild_id],['end' , '>' ,Carbon::now()],
                ['a.province_id',$province_id]
            ])
                ->join('agahi as a','fori.agahi_id',"=",'a.agahi_id')
                ->join("users as u",'a.user_id','=','u.user_id')
                ->join("product as p","a.product_id","=","p.product_id")
                ->select('a.*','u.name','u.family',"u.profile", "p.name as product_name");


            if ($request->has('type')) {
                $fories->where("a.type",$request->get('type'));
            }
            if ($request->has('search')) {
                $search = "%{$request->get('search')}%";
                $fories->where(function ($q) use ($search){
                    $q->where(DB::raw('a.title'), 'like', $search)
                        ->orWhere('a.description','like',$search)
                        ->orWhere('p.name','like',$search);
                });
            }
            $fories =  $fories->orderBy('fori_id',"desc")->get();

            foreach ($fories as $fory) {
                $fory["shamsi"]=convertTime($fory["created_at"]);
            }
            $response["fories"]=$fories;
        }else {
            $response["fories"]=array();
        }




        return $this->successReport($response,"موفق",200);
    }

    function getMyAgahies(Request $request) {
        global $user_id;
        $setting = Setting::first();
        $time_to_expire = $setting->expire *24*60*60;
        $agahies = Agahi::where('agahi.active','>=',0)->join("users as u",'agahi.user_id','=','u.user_id')->join("product as p","agahi.product_id","=","p.product_id")
            ->select('agahi.*','u.name','u.family',"p.name as product_name");
        if ($request->has('type')) {
            $agahies->where([["agahi.user_id",$user_id],["type",$request->get('type')]]);
        }else {
            $agahies->where("agahi.user_id",$user_id);
        }
        $agahies = $agahies->orderBy('agahi_id','desc')->paginate(10);
        foreach ($agahies as $agahi) {
            $agahi["shamsi"]=convertTime($agahi["created_at"]);
            $created = $agahi->created_at->timestamp;
            $current = now()->timestamp;
            $diff = $current-$created;
            if ($diff>$time_to_expire) {
                $agahi["expired"]=true;
            }else {
                $agahi["expired"]=false;
            }
        }
        return $this->successReport($agahies->items(),"موفق",200);

    }

    function getSingleAgahi(Request $request, $agahi_id){
        global $guild_id;
        $agahi = Agahi::where("agahi_id",$agahi_id)->with('images')
            ->join("users as u","agahi.user_id",'=',"u.user_id")
            ->join("product as p","agahi.product_id","=","p.product_id")
            ->select("agahi.*","u.name","u.family","u.mobile","u.store_name","u.tel_1","u.tel_2","u.mobile_visible","p.name as product_name")
            ->first();
        if ($agahi->guild_id != $guild_id) {
            return $this->failureResponse("شما اجازه مشاده این آگهی را ندارید",403);
        }
        if ($agahi->active < 0) {
            return $this->failureResponse("این آگهی وجود ندارد",404);
        }


        $report = Report::get();
        $agahi->update(['view'=>$agahi->view+1]);
        $agahi["shamsi"]=convertTime($agahi["created_at"]);
        $agahi['reports']=$report;
        return $this->successReport($agahi,"ok",200);
    }

    function reportAgahi(Request $request){
        global $user_id ;
        $rules = [
            'agahi_id'=>'required|int',
            'report_id'=>'required|int'

        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $report_user = Report_User::where([['user_id',$user_id],['agahi_id',$request->get('agahi_id')]
            ])->first();
        if ($report_user!=null) {
              return  $this->failureResponse("این آگهی قبلا توسط شما گزارش شده است",400);
        }else {
            $report_user=  Report_User::create([
                'agahi_id' => $request->get('agahi_id'),
                'report_id' => $request->get('report_id'),
                'user_id' => $user_id
            ]);
            return  $this->successReport([],"گزارش آگهی با موفقیت انجام شد",200);
        }
    }

    function deleteAgahi(Request $request,Agahi $agahi) {
        global $user_id;
        if ($agahi->user_id != $user_id){
         return  $this->failureResponse("not allowed",405);
        }
       $agahi->delete();
       return $this->successReport([],"حذف با موفقیت انجام شد",200);

    }

    function refreshAgahi(Request $request,Agahi $agahi) {
        global $user_id;
        $setting = Setting::first();
        $time_to_expire = $setting->expire *24*60*60;
        $created = $agahi->created_at->timestamp;
        $current = now()->timestamp;
        $diff = $current-$created;


        $day = Carbon::now()->dayOfWeekIso;
        $opening = Opening::where('number',$day)->first();
        if ($opening->start==1 && $opening->end==1) {
            return $this->failureResponse($opening->description,400);
        }else if ($opening->start!=0 || $opening->end!=0) {
            $current_hour = Carbon::now()->hour;
            if ($current_hour < $opening->start || $current_hour>=$opening->end) {
                return $this->failureResponse($opening->description,400);
            }
        }


        if ($diff<$time_to_expire) {
            return $this->failureResponse("آگهی هنوز منقضی نشده است",400);
        }

        $setting = Setting::first();
        $last_agahi = Agahi::where([['user_id',$user_id],['is_extra',0]])->orderBy('created_at','desc')->first();
        if (!$last_agahi==null && $setting->ad_limit>0) { //here means  that he has add list one agahi
            $created = $last_agahi->created_at->timestamp;;
            $current = now()->timestamp;
            $diff = $current-$created;
            $limit = $setting->ad_limit * 60;

            if ($diff<$limit) {
                if ($request->exists("use_package")) {
                    $user_package = UserPackage::where('user_id', $user_id)->
                    select([DB::raw("SUM(banner) as banner"), DB::raw("SUM(ad) as ad"),
                        DB::raw("SUM(special) as special"),DB::raw("SUM(notif) as notif")
                    ])
                        ->groupBy('user_id')->first();
                    if (!$user_package) {
                        return $this->failureResponse("کاربر گرامی ، شما پکیج آگهی اضافه فعالی ندارید ",406);
                    }
                    if ($user_package->ad == 0 ) {
                        return $this->failureResponse("کاربر گرامی ، شما پکیج آگهی اضافه فعالی ندارید",406);
                    }
                }else {
                    return $this->failureResponse("محدودیت زمانی شما برای ثبت آگهی " .  gmdate("i:s", $limit-$diff). " ثانیه دیگر به پایان میرسد",403);

                }


            }
        }
        $new_agahi=  Agahi::create([
            "title"=>$agahi->title,
            "user_id"=>$agahi->user_id,
            "province_id"=>$agahi->province_id,
            "product_id"=>$agahi->product_id,
            "description"=>$agahi->description,
            "type"=>$agahi->type,
            "guild_id"=>$agahi->guild_id,
            "def"=>$agahi->def,
            "pic"=>$agahi->pic,
            "is_extra"=>$request->exists("use_package") ? 1 : 0
        ]);
        Agahi_image::where('agahi_id',$agahi->agahi_id)->update(['agahi_id'=>$new_agahi->agahi_id]);
        $agahi->forceDelete();

        if ($request->exists("use_package")) {
            $user_package = UserPackage::where([["user_id",$user_id],["ad",">",0]])->first();
            $user_package->update(["ad"=>$user_package->ad-1]);
        }

        return $this->successReport([],"ارسال دوباره با موفقیت انجام شد",200);

    }

}
