<?php

namespace App\Http\Controllers\UserControllers;
use App\helper\KavehNegar;
use App\Http\Controllers\Base\BaseUser;
use App\Models\City;
use App\Models\Comment;
use App\Models\Forget;
use App\Models\Guild;
use App\Models\Notif;
use App\Models\Otp;
use App\Models\Otp_Tel;
use App\Models\Province;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use function App\helper\convertTime;


class UserController extends BaseUser
{

    function register(Request $request) {
        $rules = [
            'name' => 'required|string|max:255',
            'family' => 'required|string|max:255',
            'mobile'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'is_employment'=>'required|int|min:0|max:1',
            'password'=> 'required|min:6',
            'province_id'=>'required|int',
            'city_id'=>'required|int',
            'address'=>"required|string|max:255",
            'guild_id'=>'required|int',
            'store_name'=>'required|string|max:255'
        ];
        $validator_message=[
             'password.min'=>'رمز عبور باید حداقل دارای 6 حرف باشد',
             'mobile.regex'=>'فرمت موبایل وارد شده صحیح نمیباشد'
        ];

        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        if ($request->get('is_employment')==1) {
            $rules = [
                'e_name' => 'required|string|max:100',
                'e_family' => 'required|string|max:100',
                'e_mobile'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric'
            ];
            $validator_message=[
                'e_mobile.regex'=>'فرمت موبایل کارفرما صحیح نمیباشد'
            ];
            $validator = Validator::make($request->all(),$rules,$validator_message);
            if ($validator->fails()) {
                return $this->failureResponse($validator->errors()->first(),422);
            }
        }

        $exits = User::where([['mobile',$request->get('mobile')],['confirmed',1]])->first();
        if ($exits) {
            return $this->failureResponse("این شماره قبلا ثبت شده است",400);
        }
        User::where('mobile',$request->get('mobile'))->delete();
            $random = rand ( 1000 , 9999 );
            $otp = Otp::where('mobile',$request->get('mobile'))->first();
            if ($otp) {
                $otp->update(['otp'=>$random]);
            }else {
                Otp::create(['otp'=>$random,'mobile'=>$request->get('mobile')]);
            }
            KavehNegar::sendOtpMessage($request->get('mobile'),$random,'verify');
            $user=  User::create([
                'name' => $request->get('name'),
                'family' => $request->get('family'),
                'mobile'=>$request->get('mobile'),
                'password' => bcrypt($request->get('password')),
                'province_id'=>$request->get('province_id'),
                'city_id'=>$request->get('city_id'),
                'address'=>$request->get('address'),
                'guild_id'=>$request->get('guild_id'),
                'store_name'=>$request->get('store_name'),
                'is_employment'=>$request->get('is_employment'),
                'e_name'=>$request->has('e_name') ? $request->get('e_name') : null,
                'e_family'=>$request->has('e_family') ? $request->get('e_family') : null,
                'e_mobile'=>$request->has('e_mobile') ? $request->get('e_mobile') : null
            ]);

            $token = JWTAuth::fromUser($user);
            $user["token"]=$token;
            return $this->successReport($user,"ثبت نام با موفیت انجام گردید",201);

    }

    function verifyMobile(Request $request) {
        $rules = [
            'mobile'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'otp'=> 'required|string|min:4|max:4',
        ];
        $validator_message=[
            'mobile.regex'=>'فرمت موبایل وارد شده صحیح نمیباشد'
        ];
        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $exits = User::where([['mobile',$request->get('mobile')],['confirmed',1]])->first();
        if ($exits) {
            return $this->failureResponse("این شماره قبلا ثبت شده است",400);
        }
        $otp = Otp::where('mobile',$request->get('mobile'))->first();

        if (!$otp) {
            return $this->failureResponse('این شماره وجود ندارد',400);
        }
        $created = $otp->updated_at->timestamp;
        $current = now()->timestamp;

        if ($otp->otp != $request->get('otp')) {
            return $this->failureResponse('کد ارسال شده صحیح نمیباشد',400);
        }
        if ($current-$created > 120 ) {
            return $this->failureResponse('کد ارسال شده منقضی شده است',400);
        }

        $user = User::where('mobile',$request->get('mobile'))->first();
        $user->update(['confirmed'=>1]);
        if ($user){
            return $this->successReport([],'ثبت نام با موفقیت تکمیل گردید . لطفا منتطر تایید از طرف تیم پشتیبانی بمانید',200);
        }else {
            return $this->failureResponse('خطا در ثبت . دوباره تلاش کنید',400);
        }


    }

    function requestAgain(Request $request){
        $rules = [
            'mobile'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
        ];
        $validator_message=[
            'mobile.regex'=>'فرمت موبایل وارد شده صحیح نمیباشد'
        ];
        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $user = User::where("mobile",$request->get("mobile"))->first();
            if ($user->confirmed==1){
                return $this->failureResponse('این شماره قبلا تایید شده است ',400);
            }

        $otp = Otp::where('mobile',$request->get('mobile'))->first();
        if (!$otp) {
            return $this->failureResponse('این شماره وجود ندارد',400);
        }
        $created = $otp->updated_at->timestamp;
        $current = now()->timestamp;
        if ($current-$created < 120 ) {
            return $this->failureResponse('کد ارسالی قبل هنوز منقصی نشده است',400);
        }
        $random = rand ( 1000 , 9999 );
        $otp->update(['otp'=>$random]);
        KavehNegar::sendOtpMessage($request->get('mobile'),$random,'verify');
        if ($otp->wasChanged()) {
           return $this->successReport([],"ارسال پیام دوباره انجام شد",200);
        }else {
            return $this->failureResponse("خطا در ارسال دوباره پیام",400);
        }
    }

    function login(Request $request) {
        $rules = [
            'mobile' => 'required',
            'password'=> 'required'
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $credentials = $request->only("mobile", "password");
        if (!$token = JWTAuth::attempt($credentials)) {
          return $this->failureResponse("رمز عبور یا شماره موبایل صحیح نمیباشد",401);

        }else{
            $currentUser = Auth::user();
            $currentUser["token"]=$token;
            $city = City::where('city_id',$currentUser->city_id)->first();
            $province = Province::where('province_id',$currentUser->province_id)->first();
            $currentUser["city_name"]=$city->name;
            $currentUser["province_name"]=$province->name;

            if ($currentUser["state"]==1 && $currentUser["document"]!=null) {
                return $this->failureResponse("لطفا تا تایید مدرک ارسال شده توسط تیم پشتیبانی منتظر بمانید",400);
            }
            return $this->successReport($currentUser,"ورود با موفقیت انجام شد",200);
        }

    }




    function uploadDocument(Request $request) {
        global $user_id;
        $rules = [
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $pic = $request->pic;
        $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        $imageName = time().$s. '.'.$pic->extension();
        $directory_pic = "images/document";

        try {
          $pic->move(public_path($directory_pic), $imageName);
        }catch (\Exception $e) {
           return $this->failureResponse("خطا در بارگذاری تصویر",400);
        }
        $user = User::where('user_id',$user_id)->first();
        $user->update(['document'=>$imageName,'state'=>1]);

        if ($user->wasChanged()) {
            return $this->successReport([],'آپلود مدرک با موفقیت انجام شد . نتیجه حداکثر تا 24 ساعت آینده از طریق پیامک به شما اطلاع داده میشود ',200);
        }else {
            return $this->failureResponse('خطا در آپلود تصویر',400);
        }

    }

    //Forget Request

    function forgetRequest(Request $request){
        $rules = [
            'mobile'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
        ];
        $validator_message=[
            'mobile.regex'=>'فرمت موبایل وارد شده صحیح نمیباشد'
        ];
        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user = User::where("mobile",$request->get('mobile'))->first();
        if ($user==null) {
            return  $this->failureResponse("این شماره وجود ندارد",403);
        }

        $random = rand ( 1000 , 9999 );
        $forget = Forget::where('mobile',$request->get('mobile'))->first();
        if ($forget) {
            $forget->update(['otp'=>$random]);
        }else {
            Forget::create(['otp'=>$random,'mobile'=>$request->get('mobile')]);
        }
        KavehNegar::sendOtpMessage($request->get('mobile'),$random,'forget');
        return $this->successReport([],"ارسال پیامک فراموشی انجام شد",201);

    }

    function resetPassWord(Request $request){
        $rules = [
            'mobile'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
            'password'=> 'required|min:6',
            'otp'=> 'required|string|min:4|max:4',
        ];
        $validator_message=[
            'password.min'=>'رمز عبور باید حداقل دارای 6 حرف باشد',
            'mobile.regex'=>'فرمت موبایل وارد شده صحیح نمیباشد'
        ];
        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $forget = Forget::where('mobile',$request->get('mobile'))->first();



        if (!$forget) {
            return $this->failureResponse('این شماره وجود ندارد یا درخواستی ثبت نشده است',400);
        }
        $created = $forget->updated_at->timestamp;
        $current = now()->timestamp;

        if ($forget->otp != $request->get('otp')) {
            return $this->failureResponse('کد ارسال شده صحیح نمیباشد',400);
        }
        if ($current-$created > 120 ) {
            return $this->failureResponse('کد ارسال شده منقضی شده است',400);
        }
        $user = User::where('mobile',$request->get('mobile'))->first();
        $user->update([ 'password' => bcrypt($request->get('password'))]);
        if ($user->wasChanged()){
            return $this->successReport([],"رمز عبور با موفقیت تغییر کرد",200);
        }else {
            return $this->successReport([],"خطا در به روز رسانی",200);
        }

    }

    function requestAgainForget(Request $request){
        $rules = [
            'mobile'=>'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
        ];
        $validator_message=[
            'mobile.regex'=>'فرمت موبایل وارد شده صحیح نمیباشد'
        ];
        $validator = Validator::make($request->all(),$rules,$validator_message);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }



        $forget = Forget::where('mobile',$request->get('mobile'))->first();
        if (!$forget) {
            return $this->failureResponse('این شماره وجود ندارد',400);
        }
        $created = $forget->updated_at->timestamp;
        $current = now()->timestamp;
        if ($current-$created < 120 ) {
            return $this->failureResponse('کد ارسالی قبل هنوز منقصی نشده است',400);
        }
        $random = rand ( 1000 , 9999 );
        $forget->update(['otp'=>$random]);
        KavehNegar::sendOtpMessage($request->get('mobile'),$random,'forget');
        if ($forget->wasChanged()) {
            return $this->successReport([],"ارسال پیام دوباره انجام شد",200);
        }else {
            return $this->failureResponse("خطا در ارسال دوباره پیام",400);
        }
    }


    //tell Verify Request
    function telVerifyRequest(Request $request){
        $rules = [
            'tel'=>'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user = User::where('tel_1',$request->get('tel'))->orWhere('tel_2',$request->get('tel'))->first();
        if ($user){
            return  $this->failureResponse("این شماره قبلا ثبت شده است",400);
        }

        $random = rand ( 1000 , 9999 );
        $tel = Otp_Tel::where('tel',$request->get('tel'))->first();

        if ($tel) {
            $created = $tel->updated_at->timestamp;
            $current = now()->timestamp;
            if ($current-$created < 120 ) {
                return $this->failureResponse('کد ارسالی قبلی هنوز منقضی نشده است',400);
            }
            $tel->update(['otp'=>$random]);
        }else {
            Otp_Tel::create(['otp'=>$random,'tel'=>$request->get('tel')]);
        }
        KavehNegar::sendOtpMessage($request->get('tel'),$random,'telephone');
        return $this->successReport([],"تماس با شماره وارد شده تا لحظاتی دیگر برقرار میشود",201);

    }

    function telVerifySubmit(Request $request){
        global $user_id;
        $rules = [
            'tel'=>'required|string',
            'otp'=> 'required|string|min:4|max:4',
            'number'=>'required|int|min:1|max:2'
        ];

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $tel = Otp_Tel::where('tel',$request->get('tel'))->first();
        if (!$tel) {
            return $this->failureResponse('این شماره وجود ندارد یا درخواستی ثبت نشده است',400);
        }

        $created = $tel->updated_at->timestamp;
        $current = now()->timestamp;

        if ($tel->otp != $request->get('otp')) {
            return $this->failureResponse('کد ارسال شده صحیح نمیباشد',400);
        }
        if ($current-$created > 120 ) {
            return $this->failureResponse('کد ارسال شده منقضی شده است',400);
        }
        $user = User::where('user_id',$user_id)->first();

        $number = $request->get('number');

        $user->update(["tel_$number" => $request->get('tel')]);
        if ($user->wasChanged()){
            return $this->successReport([],"شماره با موفقیت ثبت گردید",200);
        }else {
            return $this->successReport([],"خطا در ثبت شماره",200);
        }

    }

    function telRequestAgain(Request $request){
        $rules = [
            'tel'=>'required|string',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }

        $tel = Otp_Tel::where('tel',$request->get('tel'))->first();
        if (!$tel) {
            return $this->failureResponse('این شماره وجود ندارد',400);
        }
        $created = $tel->updated_at->timestamp;
        $current = now()->timestamp;
        if ($current-$created < 120 ) {
            return $this->failureResponse('کد ارسالی قبل هنوز منقصی نشده است',400);
        }
        $random = rand ( 1000 , 9999 );
        $tel->update(['otp'=>$random]);
        KavehNegar::sendOtpMessage($request->get('tel'),$random,'telephone');
        if ($tel->wasChanged()) {
            return $this->successReport([],"ارسال درخواست تماس دوباره انجام شد",200);
        }else {
            return $this->failureResponse("خطا در ارسال درخواست",400);
        }
    }





    public function getRegisterNeed() {
        $response = array();
        $response["provinces"] = Province::where("active",1)->with('cities')->get();
        $response["guilds"] = Guild::where('active',1)->get();
        return $this->successReport($response,"دریافت با موفقیت انجام شد",200);
    }

    public function comment(Request $request){
        global $user_id;
        $rules = [
            'rate'=>'required|int|min:1|max:5',
            'comment'=> 'required|string',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
            $comment = Comment::create([
                "comment"=>$request->comment,
                "rate"=>$request->rate,
                "user_id"=>$user_id
            ]);
            return $this->successReport([],"تظر شما با موفقیت ثبت گردید",200);

    }

    public function getProfile(){
        global $user_id;
        $user = User::where('user_id',$user_id)->first();
        return $this->successReport($user,"ok",200);
    }

    public function updateProfile(Request $request){

        global $user_id;
        $rules = [
            'visible' => 'required|int|min:0|max:1'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user = User::where('user_id',$user_id)->first();
        if ($request->has('pic')) {
            $pic = $request->pic;
            $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $imageName = time().$s. '.'.$pic->extension();
            $directory_pic = "images/profile";
            try {
                $pic->move(public_path($directory_pic), $imageName);
            }catch (\Exception $e) {
                return  $this->failureResponse("خطا در بارگذاری تصویر",400);
            }
        }
        $profile = null;
        if ($request->has('pic')) {
            $profile=$imageName;
        }else if($user["profile"]!=null){
            $profile=$user["profile"];
        }
        $user->update([
            'profile'=>$profile,
            'mobile_visible'=>$request->get('visible')
            ]);
        $city = City::where('city_id',$user->city_id)->first();
        $province = Province::where('province_id',$user->province_id)->first();
        $user["city_name"]=$city["name"];
        $user["province_name"]=$province["name"];

        if ($user->wasChanged()) {
            return $this->successReport($user,"به روز رسانی با موفقیت انجام شد",201);
        }else {
           return $this->failureResponse("خطا در به روز رسانی حساب",400);
        }
    }

    public function updateFcmToken(Request $request){

        global $user_id;
        $rules = [
            'fcm_token' => 'required|string'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return $this->failureResponse($validator->errors()->first(),422);
        }
        $user = User::where('user_id',$user_id)->first();
        $user->update(['fcm_token'=>$request->get('fcm_token')]);
        if ($user->wasChanged()) {
            return $this->successReport([],"fcm token updated",200);
        }else {
            return $this->failureResponse("fcm token update failure",400);
        }
    }


    public function getNotifs() {
        global $user_id;
        $user = User::where('user_id',$user_id)->first();
        $notifs = Notif::where('user_id',$user_id)->orWhere('topic','all')->orWhere('topic',$user->guild_id)
            ->orderby('notif_id','desc')
            ->get();
        foreach ($notifs as $notif) {
            $notif["shamsi"]=convertTime($notif["created_at"]);
        }
        $user->update(["notif_read"=>count($notifs)]);
        return $this->successReport($notifs,"ok",200);
    }

    public function getUnreadNotifsCount() {
        global $user_id;
        $user = User::where('user_id',$user_id)->first();
        $count = Notif::where('user_id',$user_id)->orWhere('topic','all')->orWhere('topic',$user->guild_id)
            ->count();
        return $this->successReport($count - $user->notif_read,"ok",200);
    }

    public function logout(Request $request) {
        $token = $request->header( 'Authorization' );
        JWTAuth::parseToken()->invalidate( $token );
        return $this->successReport([],"خروج از حساب انجام شد",200);


    }

    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user()
        ]);
    }












}
