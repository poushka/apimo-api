<?php

namespace App\Http\Controllers\UserControllers;
use App\Http\Controllers\Base\BaseUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ColleageController extends BaseUser

  {
      function getColleages(Request $request) {
          return $this->failureResponse("با توجه به کمپین فروش پیش رو و  همکارانی که جدیدا درخواست عضویت دادند، احراز هویت افراد کمی زمان بر شده است ،از صبوری شما سپاس گذاریم",400);


          global $guild_id;
        $users = User::where([["guild_id",$guild_id],['active',1],['state',2]]);
        if ($request->has('city_id')){
            $users->where('city_id',$request->get('city_id'));
        }
        if ($request->has('search')) {
            $search = "%{$request->get('search')}%";

            $users->where(function ($q) use ($search){
                $q->where(DB::raw('CONCAT_WS(" ", name, family)'), 'like', $search)
               ->orWhere('store_name','like',$search);
            });

//            $users->where(DB::raw('CONCAT_WS(" ", name, family)'), 'like', $search)
//                ->orWhere('store_name','like',$search);
        }else {
            return $this->failureResponse("تمامی همکاران پیچ و مهره فروشان تهران ( میدان قزوین -شاد آباد-حسن آباد) که جواز کسب دارند از خدمات این اپلیکشن می توانند استفاده کنند ...‌ به زودی که تمامی همکاران احراز هویت شدند فعالیت اپلیکشن آغاز می شود .",400);
        }
        $users =  $users->orderBy("user_id","desc")->paginate(10);
        return $this->successReport($users->items(),"ok",200);
      }
}
