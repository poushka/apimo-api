<?php

namespace App\Http\Controllers\Base;



use App\Models\Admin;
use Illuminate\Support\Facades\Config;

class BaseAdmin extends Controller
{
    //registration
    public function __construct()
    {
        Config::set('jwt.user', Admin::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => Admin::class,
        ]]);
    }

}
