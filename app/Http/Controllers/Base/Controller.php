<?php

namespace App\Http\Controllers\Base;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

require __DIR__.'/../../../helper/jdf.php';


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function successReport($data , $message,$status) {
        // $my_response=array('data'=>$data,'message'=>$message,$status=>$status);
        $my_response=array();
        $my_response["data"]=$data;
        $my_response["message"]=$message;
        $my_response["status"]=$status;
        return response()->json($my_response,$status);
    }

//    function successReportWithCount($data , $message,$status,$count) {
//        // $my_response=array('data'=>$data,'message'=>$message,$status=>$status);
//        $my_response=array();
//        $my_response["data"]=$data;
//        $my_response["message"]=$message;
//        $my_response["status"]=$status;
//        $my_response["count"]=$count;
//        return response()->json($my_response,$status);
//    }



    function failureResponse($message,$status) {
        $my_response=array('data'=>[],'message'=>$message,"status"=>$status);
        return response()->json($my_response,$status);
    }

    function simpleRespone($status,$message) {
        return ["message"=>$message,$status=>$status];
    }
}
