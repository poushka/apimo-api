<?php

namespace App\Http\Controllers\Base;



use App\Models\User;
use Illuminate\Support\Facades\Config;

class BaseUser extends Controller
{

    //registration
    public function __construct()
    {
        Config::set('jwt.user', User::class);
        Config::set('auth.providers', ['users' => [
            'driver' => 'eloquent',
            'model' => User::class,
        ]]);

    }

}
