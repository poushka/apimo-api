<?php

namespace App\Http\Middleware;
use App\Models\Guild;
use Closure;


class agahiMiddleware
{

    public function handle($request, Closure $next)
    {
       global $guild_id;
       $guild = Guild::where('guild_id',$guild_id)->first();
       if ($guild->active==0) {
           return response()->json(['message' => 'صنف مورد نظر در حال حاضر غیر فعال میباشد','status'=>403],403);
       }
        return $next($request);
    }
}
