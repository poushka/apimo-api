<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserAuthMiddleware
{

    public function handle($request, Closure $next)
    {
      try {
            global $user_id;
            global $guild_id;
            global $province_id;
            $user = JWTAuth::parseToken()->authenticate();
            $user_id = $user->user_id;
            $guild_id = $user->guild_id;
            $province_id=$user->province_id;
            $active = $user["active"];
            if ($active==0) {
                return response()->json(['message' => 'حساب کاربری معلق شده است','status'=>403],403);
            }

            //it means that current root need privilate

        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['message' => 'Token is Invalid','status'=>401],401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['message' => 'Token is Expired','status'=>401],401);
            }else{
                return response()->json(['message' => 'Authorization Token not found','status'=>401],401);
            }
        }
        return $next($request);
    }


}
